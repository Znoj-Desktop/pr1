### **Description**
[https://znoj.cz/pr1/](https://znoj.cz/pr1/)

---
### **Technology**
C++

---
### **Year**
2017

---

## Použití referenčních vstupů a výstupů
### Windows

Otevřte si konzoli ve windows (cmd) a dostaňte se na místo, kde je
uložen binární soubor vašeho projektu:  
  
 "cesta k vašemu projektu v Code::Blocks"\\bin\\Debug\\  
 *nebo "cesta k vašemu projektu v Code::Blocks"\\bin\\Release\\ (záleží,
kterou variantu používáte v Code::Blocks při překladu programu)*  
  
 spusťte program příkazem "nazevProjektu".exe \< input \> output

### Linux

Otevřte si terminal a dostaňte se na místo, kde je uložen binární soubor
vašeho projektu:  
  
 "cesta k vašemu projektu v Code::Blocks"/bin/Debug&ltbr/\> *nebo "cesta
k vašemu projektu v Code::Blocks"/bin/Release/ (záleží, kterou variantu
používáte v Code::Blocks při překladu programu)*  
  
 ./"nazevProjektu" \< input \> output

### Windows + Linux

- stáhněte si referenční vstupy a výstup z těchto stránek 
- porovnejte váš výstup s referenčním výstupem. 
- ve windows lze použít pro porovnání výstupů např. PsPad, v Linuxu příkaz diff
    nebo lze využít i online nástroj 
    [https://www.diffchecker.com/diff](https://www.diffchecker.com/diff)

Referenční řešení jsou ve tvaru:  
referenční vstup: cvN\_jmeno\_input  
referenční výstup: cvN\_jmeno\_output  
kde: **N** = číslo školní úlohy a **jmeno** = jméno školní úlohy

### Úprava programu pro využití souborů \*\_batch

Soubory cvN\_jmeno\_input obsahují nyní více vstupů. Pokud máte zájem o
jejich plné využití, pak si upravte váš program do tvaru:  

```
int main(){
    do{  
        //váš program  
        - nahraďte příkazy "return 0" příkazy "**continue**"  
        - pokud používáte "cin.fail()", tak do podmínky (pokud dojde k chybě)
        uveďte ještě:  
        
        cin.clear(); //odstrani chybu a budeme se s ni snazit vyporadat  
        cin.ignore(256, '\\n'); //ignorujeme vsechny znaky (az 256), nebot
        jsou chybne, az dokud nedojde k odradkovani jinak se vám vždy po špatném
        vstupu program ukončí.
    }while(cin.peek() != -1);
} 
return 0;
```

Váš výstup pak můžete porovnat se souborem cvN\_jmeno\_output **\_batch**
který obsahuje výstupy pro všechny zvolené vstupy. Před odevzdáním do
progtestu opět výše uvedený cyklus while odstraňte. Pokud máte ve vašem
programu "return", pravděpodobně všechny vstupy nezpracují. Pro získání
všech výstupů nahraďtě příkazy "return 0" příkazem "continue".

#### Referenční řešení
Soubory obsažené v tomto repozitáři

#### Školní úlohy
##### 1 - Hello World

Realizujte program, který vypíše na obrazovku text "Hello world!" a
odřádkování. Text bude zobrazen bez uvozovek.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázce. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem
výstupu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí. Možná ve svém
programu používáte volání:   
  
```
#include ...

using namespace std;

int main(){ 
    ...
    return 0;
}
```


##### 2 - Dvě čísla

Vytvořte program, který načte dvě čísla a zobrazí výsledek jejich
součtu, rozdílu, součinu a podílu. Při komunikaci s uživatelem a
formátování výstupu se řiďte ukázkovými příklady níže. Neřešte možné
chyby na vstupu.   
  
 Vstupem programu jsou dvě čísla.   
  
 Výstupem programu jsou načtená čísla a komentované výsledky hodnot
základních aritmetických operací nad nimi.   
  
 Výstup čísel nijak neupravujte, mezi čísla, operátory a znaménka
vkládejte mezery, za posledním výstupem odřádkujte, jak je běžné a jak
vidíte na výpisu - jeho formátování musí být dodrženo.   
  
 Pro číselné proměnné používejte typ double. Nepoužívejte žádné funkce.
Kostra Vašeho programu bude vypadat následovně:

```
int main() {
    double a, b;
    return 0;
}
```


##### 2 - Řetězec a číslo

Vytvořte program, který načte řetězec a číslo. Program vypíše načtený
řetězec v daném tvaru a provede násobení a dělení zadaného čísla
konstantou. Výpočty pak vypíše také. Při komunikaci s uživatelem a
formátování výstupu se řiďte ukázkovými příklady níže (řetězec může
obsahovat více slov). Neřešte možné chyby na vstupu.   
  
 Vstupem programu je řetezec a celé číslo.   
  
 Výstupem programu je řetězec obsahující načtený řetězec na jednom
řádku, na druhém násobení načteného čísla konstantou a na třetím
vydělení načteného stejnou konstantou.   
  
 Zmiňovanou konstantou je číslo 3. Nepoužívejte žádné vlastní funkce.
Kostra Vašeho programu bude vypadat takto:

```
int main() {
    string name;
    int number;
    
    return 0;
}
```


##### 3 - Kružnice

Vytvořte program, který vypíše informace pro sestaveni rovnice kružnice,
jejímž průměrem je úsečka AB.   
  
 Vstupem programu je dvojice bodů A a B s x-ovou a y-ovou souřadnicí:
A[x,y], B[x,y].   
  
 Výstupem programu jsou následující informace v pořadí dle ukázek:   
  
 - X-ova souřadnice středu S: (Ax + Bx) / 2   
 - Y-ova souřadnice středu S: (Ay + By) / 2   
 - Délka směrového vektoru u: sqrt((Bx - Ax) \* (Bx - Ax) + (By - Ay) \*
(By - Ay))   
 - Poloměr r kružnice: polovina délky směrového vektoru   
  
 **Nápověda:**   
 - Pro uložení desetinných čísel používejte datový typ double,
nepoužívejte float.   
 - Druhá mocnina čísla x se vypočítá jako x \* x.   
 - Pro odmocninu použijte funkci sqrt z matematické knihovny 
    (\#include - viz příklad níže).   
 - Neočekávejte neplatné vstupy - na vstupu budou vždy čísla.   
 - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet
ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy
(viz níže) vypadat přesně tak, jako ukázky.   
 - Na konci výpisu je prázdný řádek.   
Příklad použití funkce sqrt:

```
#include <iostream>
#include <cmath>
using namespace std;

int main() {
    double number = 25, result;
    result = sqrt(number); // result ma ted hodnotu 5 
    
    return 0;
}
```


##### 3 - Tři čísla

Realizujte program, který nad třemi čísly a, b, c provede součet, součin
a rozdíl. Rozdíl bude počítán takto: a - b - c.   
  
 Vstupem programu jsou 3 celá čísla.   
  
 Výstupem programu je součet čísel, součin a rozdíl. V tomto pořadí.


##### 4 - Kvadratická rovnice

Vytvořte program, který ze zadaných parametrů a, b a c vypočítá kořeny
kvadratické rovnice.   
  
 Vstupem programu jsou tři desetinná čísla reprezentující parametry
kvadratické rovnice a, b a c.   
  
 Výstupem programu jsou kořeny kvadratické rovnice v pořadí a tvaru dle
ukázek.   
  
 **Nápověda:**   
 - Pro uložení desetinných čísel používejte datový typ double,
nepoužívejte float.   
 - Neočekávejte neplatné vstupy - na vstupu budou vždy reálná čísla.   
 - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet
ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy
(viz níže) vypadat přesně tak, jako ukázky.   
 - Na konci výpisu je prázdný řádek.   
  

##### 4 - Trojúhelník

Vytvořte program, který načte tři reálná čísla a rozhodne, zda tato
čísla představují délky stran skutečného trojúhelníka. Pokud takový
trojúhelník existuje, program vypočítá jeho obsah. Pokud ne, program to
řekne.   
  
 Vstupem programu jsou tři reálná čísla.   
  
 Výstupem programu je obsah trojúhelníka. Pokud trojúhelník neexistuje,
je výstupem informace, že trojúhelník neexistuje.   
  
 **Nápověda:**   
 - Obsah S trojúhelníka se stranami a, b, c se vypočítá podle Heronova
vzorce: S = sqrt(s \* (s - a) \* (s - b) \* (s - c)), kde s = (a + b +
c) / 2.   
 - Trojúhelník neexistuje, pokud je jeho obsah záporný.   
 - Neočekávejte neplatné vstupy - na vstupu budou vždy reálná čísla.   
 - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet
ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy
(viz níže) vypadat přesně tak, jako ukázky.   
 - Na konci výpisu je prázdný řádek.


##### 5 - Schody

Vytvořte program, který pro zadaný počet schodů vykreslí jejich obraz
podle vzoru na ukázkách níže.   
  
 Vstupem programu je jedno číslo - počet schodů.   
  
 Výstupem programu je tolik řádků, kolik odpovídá počtu schodů, schod je
tvořen znakem '\_' (podtržítko), výplňové znaky zleva znakem 'X', výstup
je na ukázkách níže.   
  
 Pozor, na vstupu se mohou vyskytnout nedovolené číselné hodnoty i
hodnoty jiných datových typů (nečíselné hodnoty). Možné chyby na vstupu
musíte ošetřit.   
  

##### 5 - Validace vstupu

Tímto programem prokážete zvládnutí ošetření chybného vstupu. Vytvořte
program, který má za úkol načíst desetinné číslo a zobrazit jej, nebo
ohlásit chybu. Podrobněji vidíte chování programu na ukázkách níže.   
  
 Vstupem programu je jedno desetinné číslo.   
  
 Výstupem programu je zadané číslo (nebo jeho část) s informací, že
vstup je v pořádku, případně (pokud bude zadáno něco jiného) ohlásí
program nesprávný vstup. Formát výstupu je v ukázkách níže. Pro
zvládnutí povinného testu stačí správně ošetřit jednodušší případ, tedy
ohlásit úspěch i při načtení části čísla. Tedy jednodušší řešení může
pro vstup 123.45.67 oznámit úspěch (načte 123.45).   
  
 Pokud Váš program dokáže správně odmítnout i takto špatně zapsaná
čísla, uspěje i v druhém nepovinném testu a získá malý bonus.   
  
 Pozor, na vstupu se mohou vyskytnout jak číselné hodnoty nebo jejich
části, tak hodnoty jiných datových typů (nečíselné hodnoty). Možné chyby
na vstupu musíte ošetřit a zapsat výstup podle vzoru, jinou funkčnost
program nemá.


##### 6 - Binární číslo

Napište program, který převádí celá čísla zapsaná ve dvojkové soustavě
na čísla v soustavě desítkové, která zapíše do výstupu.   
  
 Vstupem programu je posloupnost znaků 0 a 1, představující normální
zápis dvojkového čísla, tj. zapsané zleva doprava, nejvyšší řád je
vlevo, nejnižší vpravo. Vstup je ukončen koncem řádku. Každý řádek
vstupu obsahuje právě jedno dvojkové číslo. Pokud jsou v řetězci jiné
znaky, ohlásí program chybu a ukončí se.   
  
 Výstupem programu je desítková celočíselná hodnota přečteného binárního
čísla, nebo hlášení o chybném vstupu. Na konci výstupu je vždy nový
řádek.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše řešení považované za nesprávné. Záleží i na mezerách, i
na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(tedy i za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
  
##### 6 - While cyklus

Vytvořte program, který sčítá reálná čísla na vstupu, dokud nepřečte
hodnotu 1000. Tato hodnota se rovněž přičte k celkovému součtu, který se
zobrazí a program skončí, viz ukázka výstupu níže.   
  
 Vstupem programu jsou reálná čísla, posledním číslem je 1000.   
  
 Výstupem programu je součet zadaných reálných čísel následovaný novým
řádkem.   
  
 Na vstupu jsou pouze číselné hodnoty, poslední je 1000; netestujte
možné chyby vstupu.   
  
 Dodržte přesně formát všech výpisů podle ukázky. Využijte přiložený
archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak
využít přesměrování vstupů/výstupů k testování Vašeho programu.


##### 7 - Brigádníkův plat

Hrad má určitou návštěvnost. Na hradu je pro návštěvníky k dispozici
atrakce placená 50 korunami. 7% návštěvníků hradu tuto atrakci využije a
zaplatí. Atrakci obsluhuje brigádník, jehož plat je dán denním obratem
na atrakci:   
  
 0 - 1950 Kč.......7O Kč/h,   
 2000 - 2450 Kč....80 Kč/h,   
 2500 - 2950 Kč....90 Kč/h,   
 3000 - 3450 Kč....100 Kč/h,   
 3500 a více Kč....110 Kč/h.   
  
 Brigádník pracuje 8 hodin denně. Napište program, který pro zadaný
počet návštěvníků hradu vypočte kolik si brigádník vydělá.   
  
 Vstupem programu je přirozené číslo, představující počet návštěvníků
hradu.   
  
 Výstupem programu je odhadovaný plat brigádníka. Je-li zapotřebí
zaokrouhlování, zaokrouhlujte vždy dolů.   
  
 Program kontroluje správnost vstupů. Požadavky na správnost vstupu z
hlediska počtu zákazníků jsou zřejmé. Na libovolné neshody s požadavky
na správnost program zareaguje výpisem řetězce "Nesprávný vstup." a
ukončením.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 **Ukázka práce programu:**

Zadej pocet navstevniku:  
1000  
Brigadnik vydela: 880 Kc  

Zadej pocet navstevniku:  
100  
Brigadnik vydela: 560 Kc  

Zadej pocet navstevniku:  
523  
Brigadnik vydela: 560 Kc  

Zadej pocet navstevniku:  
1234  
Brigadnik vydela:  880 Kc  

Zadej pocet navstevniku:  
-5  
Nespravny vstup.  

Zadej pocet navstevniku:  
0  
Brigadnik vydela: 560 Kc  

Zadej pocet navstevniku:  
Vyhodili jsme brigadnika.  
Nespravny vstup.  


##### 7 - Hexadecimální číslo

Realizujte program, který převede hexadecimální číslo na decimální.   
  
 Vstupem programu je jednoslovný řetězec představující hexadecimální
číslo. Písmena hexadecimálního zápisu mohou být velká i malá.
Bezprostředně za vlastním číslem je odřádkování.   
  
 Výstupem programu je decimální hodnota vstupního čísla, ukončená znakem
pro odřádkování.   
  
 Pokud je vstupní řetězec nesprávný (obsahuje neplatné cifry, nebo za
číslem nenásleduje odřádkování, program vypíše chybové hlášení podle
ukázky a ukončí se.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

Zadejte hexadecimalni cislo:  
abeced  
Desitkove: 11267309  

Zadejte hexadecimalni cislo:  
alpha  
Nespravny vstup.  

Zadejte hexadecimalni cislo:  
AAAFF  
Desitkove: 699135  

Zadejte hexadecimalni cislo:  
ostrava  
Nespravny vstup.  

Zadejte hexadecimalni cislo:  
abc 12  
Nespravny vstup.  


##### 8 - Kombinační čísla

Realizujte program, který spočítá kombinační číslo.   
  
 Vstupem programu jsou dvě celá čísla, n a k, oddělená mezerou.   
  
 Výstupem programu je hodnota kombinačního čísla n nad k vypočítaná
podle vzorce n! / (k! \* (n - k)!). Na konci výstupu je vždy prázdný
řádek.   
  
 Program kontroluje vstupy dle platných pravidel pro výpočet
kombinačního čísla.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

Zadejte n a k:  
4 2  
C = 6  

Zadejte n a k:  
12 5  
C = 792  

Zadejte n a k:  
0 0  
C = 1  

Zadejte n a k:  
3 -2  
Nespravny vstup.  

Zadejte n a k:  
1 6  
Nespravny vstup.  

Zadejte n a k:  
hello world  
Nespravny vstup.  


##### 8 - Prvočísla

Napište program, který vypíše všechna prvočísla ze zvoleného rozsahu.   
  
 Na vstupu jsou dvě celá kladná čísla udávající začátek a konec
intervalu (včetně).   
  
 Na výstupu je seznam nalezených prvočísel, každé na novém řádku.   
  
 Program musí kontrolovat správnost vstupu. Pokud je vstup nesprávný,
zobrazte chybovou hlášku a ukončete program. Za chybu považujte:   
 - nečíselné vstupy,  
 - dolní mez vyšší než horní mez.  
  
 Uvažujte pouze meze menší než 2000000000, tedy pro ukládání hodnot
použijte datový typ int.   
  
 **Ukázka práce programu:**

Zadejte interval:  
0 10  
2  
3  
5  
7  

Zadejte interval:  
-200 15  
2  
3  
5  
7  
11  
13  

Zadejte interval:  
3 17  
3  
5  
7  
11  
13  
17  

Zadejte interval:  
1999999900 2000000000  
1999999913  
1999999927  
1999999943  
1999999973  

Zadejte interval:  
13 13  
13  

Zadejte interval:  
1 test  
Nespravny vstup.  

Zadejte interval:  
test 2  
Nespravny vstup.  

Zadejte interval:  
30 20  
Nespravny vstup.  


##### 9 - Heslo

Realizujte program, který pro zadané heslo zjistí, zda splňuje
následující podmínky:   
   
 - musí obsahovat nejméně 5 znaků,   
 - musí obsahovat alespoň jedno malé nebo velké písmeno,   
 - musí obsahovat alespoň jednu číslici,   
 - musí obsahovat alespoň jeden znak, který není číslicí nebo písmenem.
  
  
 Vstupem programu je jednoslovný řetězec (řetězec neobsahující mezery,
konce řádků ani jiné bílé znaky).   
  
 Výstupem programu je informace o tom, zda vstupní řetězec splňuje
podmínky. Na konci výstupu je odřádkování.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

abcdefgh123451\#\$@@\#!  
Heslo splnuje pozadavky.  

1a2a3a%a\^  
Heslo splnuje pozadavky.  

abcedfed  
Heslo nesplnuje pozadavky.  

a1@\#  
Heslo nesplnuje pozadavky.  

abce212  
Heslo nesplnuje pozadavky.  


##### 9 - Porovnávání řetězců

Realizujte program, který pro tři slova vypíše počet znaků, které
obsahují, a porovná každé s každým. Vypíše, zda se slova rovnají či
nikoliv.   
  
 Vstupem programu jsou tři řetězce slovo1, slovo2 a slovo3.   
  
 Výstupem programu jsou následující informace v pořadí dle ukázek:   
   
 - informace, zda se shoduje slovo1 a slovo2   
 - informace, zda se shoduje slovo1 a slovo3   
 - informace, zda se shoduje slovo2 a slovo3   
 - počet znaků ve slovo1   
 - počet znaků ve slovo2   
 - počet znaků ve slovo3   
  
 Program detekuje chybu, pokud na vstupu nejsou zadaná požadovaná tři
slova. V takovém případě vypíše chybové hlášení dle ukázky a ukončí se.
Chybové hlášení vypisujte na standardní výstup (nevypisujte jej na
standardní chybový výstup).   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

Zadejte 3 slova:  
ahoj moje prikladzevsechnejtezsi  
Slovo1 a slovo2 nejsou stejna.  
Slovo1 a slovo3 nejsou stejna.  
Slovo2 a slovo3 nejsou stejna.  
Pocet znaku ve slovo1 je: 4  
Pocet znaku ve slovo2 je: 4  
Pocet znaku ve slovo3 je: 22  

Zadejte 3 slova:  
cviceni java programovani  
Slovo1 a slovo2 nejsou stejna.  
Slovo1 a slovo3 nejsou stejna.  
Slovo2 a slovo3 nejsou stejna.  
Pocet znaku ve slovo1 je: 7  
Pocet znaku ve slovo2 je: 4  
Pocet znaku ve slovo3 je: 12  

Zadejte 3 slova:  
hello world Nespravny vstup.  

Zadejte 3 slova:  
jednodenni ale ale  
Slovo1 a slovo2 nejsou stejna.  
Slovo1 a slovo3 nejsou stejna.  
Slovo2 a slovo3 jsou stejna.  
Pocet znaku ve slovo1 je: 10  
Pocet znaku ve slovo2 je: 3  
Pocet znaku ve slovo3 je: 3  

Zadejte 3 slova:  
dva akvarium dva  
Slovo1 a Slovo2 nejsou stejna.  
Slovo1 a Slovo3 jsou stejna.  
Slovo2 a Slovo3 nejsou stejna.  
Pocet znaku ve slovo1 je: 3  
Pocet znaku ve Slovo2 je: 8  
Pocet znaku ve Slovo3 je: 3  

Zadejte 3 slova:  
malo malo malo  
Slovo1 a slovo2 jsou stejna.  
Slovo1 a slovo3 jsou stejna.  
Slovo2 a slovo3 jsou stejna.  
Pocet znaku ve slovo1 je: 4  
Pocet znaku ve slovo2 je: 4  
Pocet znaku ve slovo3 je: 4  

Zadejte 3 slova:  
dvatisicedvestedvacetdva dvatisicedvestedvacetdvaapul trista  
Slovo1 a slovo2 nejsou stejna.  
Slovo1 a slovo3 nejsou stejna.  
Slovo2 a slovo3 nejsou stejna.  
Pocet znaku ve slovo1 je: 24  
Pocet znaku ve slovo2 je: 28  
Pocet znaku ve slovo3 je: 6  

Zadejte 3 slova:  
NEMAMRAD nemamrad NEmamRAD
Slovo1 a slovo2 jsou stejna.  
Slovo1 a slovo3 jsou stejna.  
Slovo2 a slovo3 jsou stejna.  
Pocet znaku ve slovo1 je: 8  
Pocet znaku ve slovo2 je: 8  
Pocet znaku ve slovo3 je: 8  


##### 10 - Kosinová míra podobnosti

Realizujte program, který pro dva zadané vektory spočítá jejich
podobnost pomocí kosinové podobnosti (CSM - Cosine Similarity Measure).
  
  
 Vstupem programu je na prvním řádku celé číslo N udávající počet prvků
obou vektorů. Na druhém řádku se nachází N desetinných čísel udávajících
hodnoty prvního vektoru, na třetím řádku pak N desetinných čísel
udávajících hodnoty druhého vektoru.   
  
 Výstupem programu je kosinus úhlu mezi vstupními vektory. Výpočet
kosinové podobnosti je snadný - označíme-li si vektory v1 a v2, je
podobnost dána vztahem   
(v1 \* v2) / (||v1|| \* ||v2||).   
 Na konci výstupu je vždy odřádkování.   
  
 Program kontroluje správnost vstupů. Počet prvků vektoru nesmí být
záporný ani nula, ostatní parametry mohou nabývat libovolné hodnoty
desetinného čísla. Je-li vstup chybný, vypíše program řetězec "Nesprávný
vstup." a ukončí se.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

5  
12 15 0 1 5  
0.12 0.14 1.54 2 14  
CSM: 0.267  

5  
0 0 0 1 2  
0.1 0 0 0 1  
CSM: 0.890  

12  
0 1 2 3 4 5 6 7 8 9 10 11  
11 10 9 8 7 6 5 4 3 2 1 0  
CSM: 0.435  

5  
5.12 9.21 9.68 14.54 0.1  
5.43 6.43 6.57 540 test  
Nespravny vstup.  

-5  
Nespravny vstup.  


##### 10 - Průnik množin

Realizujte program, který pro dvě množiny určí jejich PRŮNIK.   
  
 Vstupem programu je počet prvků množiny a prvky množiny (celá čísla).
  
  
 Výstupem programu jsou vypsané prvky průniku zadaných dvou množin.
Pořadí prvků ve výpisu není podstatné, prvky se ale ve výpisu nesmí
opakovat (jsou to množiny).   
  
 Program detekuje chybu, zobrazí chybové hlášení dle ukázky a ukončí se,
pokud jsou na vstupu nečíselné hodnoty nebo pokud nějaká vstupní množina
obsahuje stejný prvek vícekrát (jsou to množiny!). Chybové hlášení
vypisujte na standardní výstup (nevypisujte jej na standardní chybový
výstup).   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
 **Ukázka práce programu:**

Zadejte pocet prvku mnoziny A:  
5  
Zadejte prvky mnoziny A:  
1 3 7 9 4  
Zadejte pocet prvku mnoziny B:  
6  
Zadejte prvky mnoziny B:  
15 1 4 99 3 17  
Prunik mnozin:  
{1, 3, 4}  

Zadejte pocet prvku mnoziny A:  
7  
Zadejte prvky mnoziny A:  
1 2 3 4 5 6 7  
Zadejte pocet prvku mnoziny B:  
7  
Zadejte prvky mnoziny B:  
6 4 5 2 3 1 7  
Prunik mnozin:  
{1, 2, 3, 4, 5, 6, 7}  

Zadejte pocet prvku mnoziny A:  
4  
Zadejte prvky mnoziny A:  
10 8 27 16  
Zadejte pocet prvku mnoziny B:  
2  
Zadejte prvky mnoziny B:  
33 17  
Prunik mnozin:  
{}  

Zadejte pocet prvku mnoziny A:  
5  
Zadejte prvky mnoziny A:  
1 8 12 6 8  
Nespravny vstup.  

Zadejte pocet prvku mnoziny A:  
-3  
Nespravny vstup.  

Zadejte pocet prvku mnoziny A:  
2  
Zadejte prvky mnoziny A:  
5 abcd  
Nespravny vstup.  


##### 11 - Lokalizace robota

Realizujte program, který simuluje pohyb robota výpočtem souřadnic, na
kterých se robot bude v každém kroku nacházet, a úhlu, v jakém robot
bude natočen.   
  
 Vstupem programu je několik parametrů. Prvním z nich je kladné celé
číslo na prvním řádku označující počet iterací, kolikrát se provede
výpočet souřadnic a úhlu. Na druhém řádku je pak 6 desetinných čísel
udávající v tomto pořadí následující parametry: počáteční souřadnice x,
počáteční souřadnice y, počáteční natočení (úhel) robota fi, časový
rozdíl mezi jednotlivými kroky výpočtu delta, rychlost pohybu v a úhel,
o který se robot natočí po jednom kroku omega.   
  
 Výstupem programu jsou informace o tom, na jakých souřadnicích a s
jakým natočením se robot nachází po každém kroku. Formát výstupu viz.
ukázky. Vzorce pro výpočet nových parametrů v každém kroku jsou tyto:   
 x' = x + v \* delta \* cos(fi),   
 y' = y + v \* delta \* sin(fi),   
 fi' = fi + delta \* omega.   
 Parametry s čárkou označují nové hodnoty. Po skončení každého kroku
(iterace) se nové hodnoty stávají hodnotami výchozími pro další výpočet
(tj. hodnoty bez čárky). Vyjde-li úhel fi' mimo interval 0 až 2\*π,
přičtěte/odečtěte periodu 2\*π tak, aby se do tohoto intervalu vešel.
Pracujte s konstantou M\_PI z matematické knihovny. Za každým zobrazeným
řádkem je znak odřádkování (\\n).   
  
 Program kontroluje správnost vstupů. Počet iterací nesmí být záporný
ani nula, ostatní parametry mohou nabývat libovolné hodnoty desetinného
čísla. Je-li vstup chybný, vypíše program řetězec "Nesprávný vstup." a
ukončí se.   
  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 **Ukázka práce programu:**

5  
12 15 0 1 5 0.12  
x: 17.00, y: 15.00, fi: 0.12  
x: 21.96, y: 15.60, fi:0.24  
x: 26.82, y: 16.79, fi: 0.36  
x: 31.50, y: 18.55, fi: 0.48  
x: 35.94, y: 20.86, fi: 0.60  

10  
0 0 0 1 2 0.1  
x: 2.00, y: 0.00, fi: 0.10  
x: 3.99, y: 0.20, fi: 0.20  
x: 5.95, y: 0.60, fi: 0.30  
x: 7.86, y: 1.19, fi: 0.40  
x: 9.70, y: 1.97, fi: 0.50  
x: 11.46, y: 2.93, fi: 0.60  
x: 13.11, y: 4.06, fi: 0.70  
x: 14.64, y: 5.34, fi: 0.80  
x: 16.03, y: 6.78, fi: 0.90  
x: 17.28, y: 8.34, fi: 1.00  

-5  
Nespravny vstup.  

5  
error  
Nespravny vstup.  


##### 11 - Soubory s čísly

Realizujte funkci (ne celý program, pouze jednu funkci), která načte
čísla ze zadaného souboru, vytvoří nový soubor a do vytvořeného nového
souboru zapíše nejprve čísla sudá a za ně čísla lichá.   
  
 Funkce bude mít rozhraní:   
  
 **int evenOdd ( const char \* srcFileName, const char \* dstFileName);**   
  
 **srcFileName**  
  je řetězec se jménem zdrojového souboru. Soubor obsahuje čísla zapsaná
v desítkové podobě, na řádce je vždy jedno číslo.   
  
 **dstFileName**  
  je řetězec se jménem cílového souboru. Funkce do tohoto souboru zapíše
čísla čtená ze zdrojového souboru, ale nejprve čísla sudá (zachová
pořadí ve kterém byla ve zdrojovém souboru) a za ně pak čísla lichá
(opět zachová pořadí ve zdrojovém souboru).   
  
 **návratová hodnota**  
  funkce vrací hodnotu 1 pro úspěch (soubory se podařilo zpracovat) nebo
0 (chyba při práci se soubory).   
  
 Dodržte přesně rozhraní funkce evenOdd. Do odevzdávaného zdrojového
souboru vložte Vaší implementovanou funkci evenOdd a případné další Vaše
podpůrné funkce, které pro správný chod evenOdd potřebujete. Vkládání
hlavičkových souborů a funkci main ponechte v bloku podmíněného
překladu, jak je ukázáno v následující ukázce. Doporučujeme zkopírovat
si šablonu do Vašeho projektu a doplnit pouze požadované implementace
funkcí.

```
#ifndef __PROGTEST__
#include <iostream>
#include <cstdlib>
#endif /* __PROGTEST__ */

/* Vase pomocne funkce (jsou-li potreba) */

int evenOdd ( const char * srcFileName, const char * dstFileName )
 { 
   /* implementace */
 }

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
 {
   /* Vase testy */
   return 0;
 }
#endif /* __PROGTEST__ */
```

**Ukázka použití funkce:**

/* file in.txt:  
------8<-----  
3  
6  
7  
2  
0  
20  
19  
0  
-3  
451  
------8<-----  
*/  
evenOdd ( "in.txt", "out.txt" ); /* return: 1 */  
/* file out.txt:  
------8<-----  
6  
2  
0  
20  
0  
3  
7  
19  
-3  
451  
------8<-----  
*/  
  
/* file in.txt:  
------8<-----  
2  
6  
-4  
------8<-----  
*/  
evenOdd ( "in.txt", "out.txt" ); /* return: 1 */  
/* file out.txt:  
------8<-----  
2  
6  
-4  
------8<-----  
*/  
  
/* file in.txt:  
------8<-----  
7  
hello  
------8<-----  
*/  
evenOdd ( "in.txt", "out.txt" ); /* return: 0 */  


##### 12 - Struktury - knihovna

Realizujte program, který bude simulovat mini knihovnu o pěti knihách
:).   
  
 Vstupem programu je název knihy, příjmení autora, jméno autora, žánr,
rok vydání, cena a id. Název knihy, příjmení a jméno autora a žánr knihy
reprezentujte jako řetězce (délka 29 znaků postačuje). Rok vydání, cenu
a id reprezentujte jako celá čísla. Informace o jedné knize seskupte do
struktury.   
  
 Výstupem programu jsou následující informace v pořadí dle ukázek:   
  
 - vypíše názvy všech románů,  
 - vypíše názvy všech knih, které mají cenu menší než 300,- Kč,  
 - vypíše příjmení všech autorů.  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém).   
  
 **Ukázka práce programu:**
________________________________________  
  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Broucci Karafiat Jan pohadky 1975 150 121213  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Anatom Andahazi Federico roman 1999 250 152698  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Sedmero_pohadek Paustovskij K.G. pohadky 1974 40 235987  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Cinska_nevesta Putney M.J. roman 2001 110 25975  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Hrisnik Hunter M. roman 2008 120 45456487  
Romany jsou:  
Anatom  
Cinska_nevesta  
Hrisnik  
  
Knihy s cenou mensi nez 300,- Kc jsou:  
Broucci  
Anatom  
Sedmero_pohadek  
Cinska_nevesta  
Hrisnik  
  
Prijmeni vsech autoru jsou:  
Karafiat  
Andahazi  
Paustovskij  
Putney  
Hunter  
________________________________________  
  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Fontana_pro_Zuzanu Gasparova E. roman 1975 30 2555959  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Svudkyne Johnson S. roman 2009 130 2598742  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Indocinou_na_kole Cerveny A. cestopis 2004 99 6589456  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Tanec_s_draky Martin G.R.R roman 2012 350 11111111  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Vratne_lahve Sverak Z. povidka 2007 70 22222  
Romany jsou:  
Fontana_pro_Zuzanu  
Svudkyne  
Tanec_s_draky  
  
Knihy s cenou mensi nez 300,- Kc jsou:  
Fontana_pro_Zuzanu  
Svudkyne  
Indocinou_na_kole  
Vratne_lahve  
  
Prijmeni vsech autoru jsou:  
Gasparova  
Johnson  
Cerveny  
Martin  
Sverak  
________________________________________  
  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Laska_v_nebezpeci Quinn J. roman 2011 69 26548  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Cesta_kolem_sveta_za_80_dni Verne J. roman 1971 150 66585  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Hra_o_truny Martin G.R.R sci-fi 2011 160 789878  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
1984  Orwel G. sci-fi 2009 220 121212  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
451_stupnu_Fahrenheita Bradbury R.D. sci-fi 1957 150 363938  
Romany jsou:  
Laska_v_nebezpeci  
Cesta_kolem_sveta_za_80_dni  
  
Knihy s cenou mensi nez 300,- Kc jsou:  
Laska_v_nebezpeci  
Cesta_kolem_sveta_za_80_dni  
Hra_o_truny  
1984  
451_stupnu_Fahrenheita  
  
Prijmeni vsech autoru jsou:  
Quinn  
Verne  
Martin  
Orwel  
Bradbury  
________________________________________  
  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Hrbitov_zviratek King S. horor 1994 250 654789  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
My_deti_ze_stanice_ZOO Felscherinov C.V. roman 2005 150 36987  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Ja_robot Asimov I. sci-fi 1993 250 123456  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Robinson_Crusoe Defoe D. dobrodruzne 1961 150 35489  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Saturnin Jirotka Z. humor 2008 99 369852  
Romany jsou:  
My_deti_ze_stanice_ZOO  
  
Knihy s cenou mensi nez 300,- Kc jsou:  
Hrbitov_zviratek  
My_deti_ze_stanice_ZOO  
Ja_robot  
Robinson_Crusoe  
Saturnin  
  
Prijmeni vsech autoru jsou:  
King  
Felscherinov  
Asimov  
Defoe  
Jirotka  
________________________________________  
  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Kmotr Puzo M. roman 1990 150 568953  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Temna_vez King S. sci-fi 2010 300 22556  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Pycha_a_predsudek Austen J. romany 2008 250 398456  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Lolita Nabokov V. roman 2007 300 25478  
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:  
Zaklinac_1 Sapkowski A. sci-fi 1999 250 2458565  
Romany jsou:  
Kmotr  
Lolita  
  
Knihy s cenou mensi nez 300,- Kc jsou:  
Kmotr  
Pycha_a_predsudek  
Zaklinac_1  
  
Prijmeni vsech autoru jsou:  
Puzo  
King  
Austen  
Nabokov  
Sapkowski  
________________________________________

##### 12 - Struktury - pacienti

Realizujte program, který z pěti daných pacientů vybere všechny pacienty
s nemocí tbc a všechny pacienty s pojišťovnou 211. Nakonec vypíše
všechna příjmení pacientů.   
  
 Vstupem programu jsou příjmení, jméno, rodné číslo, nemoc a zdravotní
pojišťovna pacienta. Jméno, příjmení a diagnóza jsou řetězce (max. 50
znaků), rodné číslo a zdravotní pojišťovna jsou celá čísla.   
  
 Výstupem programu jsou následující informace v pořadí dle ukázek:   
  
 - jméno a příjmení pacientů s tbc,  
 - jméno a příjmení pacientů s pojišťovnou 211,  
 - příjmení všech pacientů.  
 Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
(a za případným chybovým hlášením). Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupů/výstupů k testování Vašeho programu.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém).   
  
 **Ukázka práce programu:**
________________________________________  
  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Novak Jan 5750113312 tbc 211  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Malinkata Ursula 6251139924 tbc 111  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Holy Tomas 5404093897 tbc 222  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Maly Xavier 6901013225 tbc 205  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Schwarzova Ludmila 7756149945 tbc 213  
Jmeno a prijmeni pacienta s tbc:  
Jan Novak  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Jan Novak  
Jmeno a prijmeni pacienta s tbc:  
Ursula Malinkata  
Jmeno a prijmeni pacienta s tbc:  
Tomas Holy  
Jmeno a prijmeni pacienta s tbc:  
Xavier Maly  
Jmeno a prijmeni pacienta s tbc:  
Ludmila Schwarzova  
Prijmeni vsech pacientu jsou:  
Novak  
Malinkata  
Holy  
Maly  
Schwarzova  
________________________________________  
  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Novakova Jana  3756111318 nestovice 205  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Ondrak Matej 1025367810 nestovice 211  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Surny Jakub 3691215181 nestovice 213  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Hotovy Karel 2136457890 nestovice 212  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Samarova Ludmila 2013597825 nestovice 212  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Matej Ondrak  
Prijmeni vsech pacientu jsou:  
Novakova  
Ondrak  
Surny  
Hotovy  
Samarova  
________________________________________  
  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Hubova Jana 1026893247 cholera 211  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Kamarad Jan 9685236741 cholera 213  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Hodny Antonin 980356301 cholera 213  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Malirova Dorota 736587410 cholera 212  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Dobra Lenka 3678214510 cholera 205  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Jana Hubova  
Prijmeni vsech pacientu jsou:  
Hubova  
Kamarad  
Hodny  
Malirova  
Dobra  
________________________________________  
  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Hruba Kamila 3693693612 zloutenka 205  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Stoural Havel 2013846361 zloutenka 212  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Lukavska Sofie 2223336689 zloutenka 213  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Modra Libuse 4478963752 zloutenka 211  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Smutny Petr 8246930178 zloutenka 205  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Libuse Modra  
Prijmeni vsech pacientu jsou:  
Hruba  
Stoural  
Lukavska  
Modra  
Smutny  
________________________________________  
  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Hodlova Karla 2530194752 chripka 211  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Pokorny Daniel 1597534562 chripka 212  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Podla Katerina 3698521470 chripka 213  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Mala Simona 3416285790 chripka 205  
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:  
Sikovny Pavel 8974236852 chripka 211  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Karla Hodlova  
Jmeno a prijmeni pacientu s pojistovnou 211 je:  
Pavel Sikovny  
Prijmeni vsech pacientu jsou:  
Hodlova  
Pokorny  
Podla  
Mala  
Sikovny  
________________________________________

#### Domácí úlohy

##### 1 - Obsah a obvod 2D obrazců

Realizujte program, který vypočítá podle volby obsah a obvod rovinného
obrazce.   
  
 Vstupem programu je znak 'a', 'b' nebo 'c', který označuje volbu
uživatele - a - čtverec, b - obdélník, c - kruh, a hodnoty nutné pro
výpočet obvodu a obsahu zvoleného obrazce.   
  
 Výstupem programu jsou následující informace v pořadí dle ukázek:   
  
 - obsah obrazce a  
 - obvod obrazce.  
 Program detekuje chybu, zobrazí chybové hlášení dle ukázky a ukončí se,
pokud jsou na vstupu nečíselné hodnoty nebo pokud rozměry (stran,
poloměr) nejsou kladná (desetinná) čísla. Chybové hlášení vypisujte na
standardní výstup (nevypisujte jej na standardní chybový výstup).   
  
 Dodržte přesně formát všech výpisu. Výpis Vašeho programu musí přesně
odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na
přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního
výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách,
i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu
a za případným chybovým hlášením. Využijte přiložený archiv s
testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít
přesměrování vstupu/výstupu k testování Vašeho programu.   
  
 **Ukázka práce programu:**

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
a  
Zadejte stranu ctverce:  
5.5  
Obsah ctverce je: 30.2500  
Obvod ctverce je: 22.0000  
  
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
b  
Zadejte strany obdelniku:  
2 3  
Obsah obdelniku je: 6.0000  
Obvod obdelniku je: 10.0000  
  
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
c  
Zadejte polomer kruznice:  
10  
Obsah kruznice je: 314.1593  
Obvod kruznice je: 62.8319  
  
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
a  
Zadejte stranu ctverce:  
-8  
Nespravny vstup.  
  
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
a  
Zadejte stranu ctverce:  
hello  
Nespravny vstup.  
  
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh  
d  
Nespravny vstup.

##### 2 - Součet binárních čísel

Sečtěte dvě čísla a vypište jejich součet, vše ve dvojkové soustavě.   
  
 Vstupem programu jsou dvě čísla ve dvojkové soustavě, čísla jsou
oddělena bílým znakem. Pokud vstup obsahuje jiné, než dvojkové hodnoty
či oddělovače, jedná se o chybu. Čísla jsou zadaná obvyklým způsobem
zleva doprava (nejvyšší řád je vlevo).   
  
 Výstupem programu je dvojkové číslo, které je součtem vstupních hodnot.
Ve výpisu potlačte nevýznamné nuly vlevo.   
  
 Chybné vstupy program detekuje. reaguje na ně výpisem chybového hlášení
a ukončením.   
  
 **Ukázka práce programu:**

Zadejte dve binarni cisla:  
10101 101001  
Soucet: 111110  
  
Zadejte dve binarni cisla:  
1111100 100  
Soucet: 10000000  
  
Zadejte dve binarni cisla:  
101 101  
Soucet: 1010  
  
Zadejte dve binarni cisla:  
11111 111  
Soucet: 100110  
  
Zadejte dve binarni cisla:  
001 000001  
Soucet: 10  
  
Zadejte dve binarni cisla:  
10020 1001a0  
Nespravny vstup.  
  
Zadejte dve binarni cisla:  
abraka1dabra0 1fuj0tajksl1  
Nespravny vstup.  


##### 3 - Velikonoce, velikonoce přicházejí, ...

Realizujte funkci easterReport, která pro zadané roky zjisti data
Velikonoc. Funkce uloží výsledný den, měsíc (v textové podobě) a rok
jako řádek tabulky do výstupního HTML souboru. Nepovinné: funkci a
veškeré pomocné funkce zdrojového souboru okomentujte pomocí nástroje
Doxygen.   
  
 Úkolem je realizovat funkci s následujícím rozhraním:   
  
 **int easterReport ( const char \* years, const char \* outFileName )**
  
**years**  
  je vstupní parametr vaší funkce. Jedná se o řetězec, který určuje
roky, pro které má funkce vypočítat data Velikonoc. Roky mohou být
zadané buď jednotlivě nebo jako intervaly. řetězec může obsahovat více
požadovaných roků/intervalů, takové jsou oddělené čárkou. Bílé znaky
mimo letopočty ignorujte. Každý rok musí být vetší než 1582 (přechod na
Gregoriánský kalendář) a menší než 2200. Pokud je zadaný interval, pak
počáteční rok intervalu musí být menší nebo roven koncovému roku
intervalu. Pokud je zadaný nesprávný vstup, funkce nic nedělá a vrací
odpovídající chybový kód (viz níže).   
  
**outFileName**  
  je řetězec se jménem výstupního souboru. Jméno souboru se může skládat
z číslic, písmen, znaků tečka, dopředné a zpětné lomítko. Výstupní
soubor navíc musí mít příponu ".html". Pokud jméno výstupního souboru
nesouhlasí, funkce nic nedělá a vrací odpovídající chybový kód (viz
níže). Pokud je jméno souboru správně, funkce do něj vygeneruje
požadovaný HTML výstup. Pro každý z roků uvedených na vstupu funkce bude
existovat jeden řádek tabulky v tomto vygenerovaném souboru.   
  
**návratová hodnota**  
  návratovou hodnotou funkce je rozlišení úspěchu/neúspěchu volání. Jsou
definované následující chybové kódy:   
    - EASTER\_OK pro úspěšné dokončení funkce.  
    - EASTER\_INVALID\_FILENAME bude vráceno, pokud jméno výstupního
souboru neodpovídá kritériím výše.  
    - EASTER\_INVALID\_YEARS bude vráceno pokud vstupní řetězec nebyl
správný (obsahoval špatně formátované roky/intervaly/nečíselné hodnoty
...).  
    - EASTER\_IO\_ERROR bude vráceno, pokud při zpracování výstupního
souboru došlo k I/O chybě.   
  Funkce nejprve kontroluje správnost jména výstupního souboru. Teprve
pokud projde, kontroluje se řetězec s roky. Tedy pokud by funkce byla
zavolaná s neplatným seznamem roků i s neplatným jménem souboru, vracela
by hodnotu EASTER\_INVALID\_FILENAME.   
  
 Algoritmus pro výpočet (Meeus/Jones/Butcher algorithm): Zadaný rok z
Gregoriánského kalendáře označme Y. Pro výpočet data Velikonoc je použit
následující algoritmus:  
 1. Y vydělíme 19 a získáme podíl (ten ignorujeme) a zbytek po dělení
označíme A. To je pozice roku v 19-letém lunárním cyklu. (A+1 je tzv.
Zlaté číslo)  
 2. Y vydělíme 100 a získáme podíl B a zbytek C  
 3. B vydělíme 4 a získáme podíl D a zbytek E  
 4. B + 8 vydělíme 25 a získáme podíl F  
 5. (B - F + 1) vydělíme 3 a získáme podíl G  
 6. 19A + B – D – G + 15 vydělíme 30 a získáme podíl (ignorujeme) a
zbytek H  
 7. C vydělíme 4 a získáme podíl I a zbytek K  
 8. (32 + 2E + 2I - H - K) vydělíme 7 a získáme podíl (ignorujeme) a
zbytek L  
 9. (A + 11H + 22L) vydělíme 451 a získáme podíl M  
 10. (H + L - 7M + 114) vydělíme 31 a získáme podíl N a zbytek P.  
 11. Velikonoční neděle je (P+1)-tý den a N-tý měsíc (N=3 pro březen a
N=4 pro duben) v roce Y.   
  
 pozn.: Je použito celočíselné dělení.   
  
 Odevzdávejte zdrojový kód obsahující Vaší implementaci požadované
funkce easterReport. V odevzdávaném zdrojovém souboru musí být
implementace této funkce a implementace všech Vašich dalších pomocných
funkcí. Vkládání hlavičkových souborů a funkci main ponechte v bloku
podmíněného překladu, jak je ukázáno v následující ukázce. Doporučujeme
zkopírovat si šablonu do Vašeho projektu a doplnit požadované
implementace funkcí.

```
#ifndef __PROGTEST__  
#include <cstdio>  
#include <cstdlib>  
#include <cctype>  
#include <cstring>  
#include <iostream>  
#include <iomanip>  
#include <fstream>  
#include <sstream>  
#include <string>  
using namespace std;  
  
#define EASTER_OK                0  
#define EASTER_INVALID_FILENAME  1  
#define EASTER_INVALID_YEARS     2  
#define EASTER_IO_ERROR          3  
  
  
#endif /* __PROGTEST__ */  
  
int easterReport ( const char * years, const char * outFileName )  
 {  
   /* todo */  
 }  
  
#ifndef __PROGTEST__  
int main ( int argc, char * argv[] )  
 {  
   /* tests */  
   return 0;  
 }  
#endif /* __PROGTEST__ */  
```

Dodržte přesně formát výstupního souboru i chybových hlášení Výstupní
soubor musí přesně odpovídat ukázkám. Testování provádí stroj, který
kontroluje obsah na přesnou shodu. Pokud se obsah Vašeho výstupního
souboru liší od referenčního výstupu,je Vaše odpověď považovaná za
nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na
odřádkování za posledním řádkem výstupu. Využijte přiložený archiv
obsahující požadovaný výstup pro ukázkové vstupy.   
  
 Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen
dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena
i velikost dostupné paměti (ale tato úloha by ani s jedním omezením
neměla mít problém). Testovací prostředí dále zakazuje používat některé
"nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí,
... Pokud jsou tyto funkce použité, program se nespustí.   
  
**Ukázka práce programu:**
 
easterReport ( "2012,2013,2015-2020", "out0.html" ); /* return: 0 */  

```
/* file out0.html:  
------8<-----  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title>C++</title>  
</head>  
<body>  
<table width="300">  
<tr><th width="99">den</th><th width="99">mesic</th><th width="99">rok</th></tr>  
<tr><td>8</td><td>duben</td><td>2012</td></tr>  
<tr><td>31</td><td>brezen</td><td>2013</td></tr>  
<tr><td>5</td><td>duben</td><td>2015</td></tr>  
<tr><td>27</td><td>brezen</td><td>2016</td></tr>  
<tr><td>16</td><td>duben</td><td>2017</td></tr>  
<tr><td>1</td><td>duben</td><td>2018</td></tr>  
<tr><td>21</td><td>duben</td><td>2019</td></tr>  
<tr><td>12</td><td>duben</td><td>2020</td></tr>  
</table>  
</body>  
</html>  
------8<-----  
*/  
```
  
easterReport ( "2000 - 2014", "out1.html" ); /* return: 0 */  
```
/* file out1.html:  
------8<-----  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title>C++</title>  
</head>  
<body>  
<table width="300">  
<tr><th width="99">den</th><th width="99">mesic</th><th width="99">rok</th></tr>  
<tr><td>23</td><td>duben</td><td>2000</td></tr>  
<tr><td>15</td><td>duben</td><td>2001</td></tr>  
<tr><td>31</td><td>brezen</td><td>2002</td></tr>  
<tr><td>20</td><td>duben</td><td>2003</td></tr>  
<tr><td>11</td><td>duben</td><td>2004</td></tr>  
<tr><td>27</td><td>brezen</td><td>2005</td></tr>  
<tr><td>16</td><td>duben</td><td>2006</td></tr>  
<tr><td>8</td><td>duben</td><td>2007</td></tr>  
<tr><td>23</td><td>brezen</td><td>2008</td></tr>  
<tr><td>12</td><td>duben</td><td>2009</td></tr>  
<tr><td>4</td><td>duben</td><td>2010</td></tr>  
<tr><td>24</td><td>duben</td><td>2011</td></tr>  
<tr><td>8</td><td>duben</td><td>2012</td></tr>  
<tr><td>31</td><td>brezen</td><td>2013</td></tr>  
<tr><td>20</td><td>duben</td><td>2014</td></tr>  
</table>  
</body>  
</html>  
------8<-----  
*/
```
  
easterReport ( "2001 , 2002  ,  2003 ,2005,2006", "out2.html" ); /* return: 0 */  
```
/* file out2.html:  
------8<-----  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title>C++</title>  
</head>  
<body>  
<table width="300">  
<tr><th width="99">den</th><th width="99">mesic</th><th width="99">rok</th></tr>  
<tr><td>15</td><td>duben</td><td>2001</td></tr>  
<tr><td>31</td><td>brezen</td><td>2002</td></tr>  
<tr><td>20</td><td>duben</td><td>2003</td></tr>  
<tr><td>27</td><td>brezen</td><td>2005</td></tr>  
<tr><td>16</td><td>duben</td><td>2006</td></tr>  
</table>  
</body>  
</html>  
------8<-----  
*/  
```
  
easterReport ( "2000,2011,2010-2020", "out3.html" ); /* return: 0 */  
```
/* file out3.html:  
------8<-----  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title>C++</title>  
</head>  
<body>  
<table width="300">  
<tr><th width="99">den</th><th width="99">mesic</th><th width="99">rok</th></tr>  
<tr><td>23</td><td>duben</td><td>2000</td></tr>  
<tr><td>24</td><td>duben</td><td>2011</td></tr>  
<tr><td>4</td><td>duben</td><td>2010</td></tr>  
<tr><td>24</td><td>duben</td><td>2011</td></tr>  
<tr><td>8</td><td>duben</td><td>2012</td></tr>  
<tr><td>31</td><td>brezen</td><td>2013</td></tr>  
<tr><td>20</td><td>duben</td><td>2014</td></tr>  
<tr><td>5</td><td>duben</td><td>2015</td></tr>  
<tr><td>27</td><td>brezen</td><td>2016</td></tr>  
<tr><td>16</td><td>duben</td><td>2017</td></tr>  
<tr><td>1</td><td>duben</td><td>2018</td></tr>  
<tr><td>21</td><td>duben</td><td>2019</td></tr>  
<tr><td>12</td><td>duben</td><td>2020</td></tr>  
</table>  
</body>  
</html>  
------8<-----  
*/
```


#### Užitečné funkce
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">  
<html> 
<body>  
<table width="300">  
<tr>
    <th>funkce</th>
    <th>popis</th>
</tr>

<tr>
    <td>getline(cin, name)</td>
    <td>načte celý řádek (stirng s; cin >> s; // načte text pouze do první mezery)</td>
</tr>
<tr>
    <td>int i = 123; int *ptr_i; ptr_i = &i; *ptr_i = 456; cout << i; //456 </td>
    <td>práce s ukazateli</td>
</tr>
<tr>
    <td>setw(10) z knihovny <iomanip></td>
    <td>zarovná číslo na 10 pozic; cout << setw(10) << 77 << endl; //"8mezer"77</td>
</tr>
<tr>
    <td>cin.fail()</td>
    <td>true, pokud selže cin. Např. int i;cin >> i; // => 'cin.fail()' bude true, pokud na vstupu bude nečíselná hodnota</td>
</tr>
<tr>
    <td>cin.peek()</td>
    <td>vrátí znak ze vztupu, aniž by jej zpracoval "jen se na něj podívá"</td>
</tr>
<tr>
    <td>cin.clear()</td>
    <td>odstrani chybu - musíme se s ni sami vyporadat</td>
</tr>
<tr>
    <td>cin.ignore(256, '\n')</td>
    <td>ignorují se všechny znaky (až 256) až dokud nedojde k odřádkování</td>
</tr>
<tr>
    <td><a href = "http://www.cplusplus.com/reference/cmath/round/">round(double x)</a> z knihovny <math.h></td>
    <td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
</tr>
<tr>
    <td>float roundf(float x)</a> z knihovny <math.h></td>
    <td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
</tr>
<tr>
<tr>
    <td>long double roundl(long double x)</a> z knihovny <math.h></td>
    <td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
</tr>
<tr>
    <td>floor() z knihovny <math.h></td>
    <td>floor(3.8) dá hodnotu 3, floor(-3.8) dá hodnotu -4</td>
</tr>
<tr>
    <td>ceil() z knihovny <math.h></td>
    <td>ceil(3.2) dá hodnotu 4, ceil(-3.2) dá hodnotu -3</td>
</tr>
<tr>
    <td>trunc() z knihovny <math.h></td>
    <td>trunc(3.2) dá hodnotu 3, ceil(-3.2) dá hodnotu -3</td>
</tr>
<tr>
    <td><i>myString</i>.find('c')</td>
    <td>Vrací pozici prvního nalezeného znaku 'c' ve stringu <i>myString</i> nebo '-1' (pokud nenalezen)</td>
</tr>
<tr>
    <td>slovo1.compare(slovo2)</td>
    <td>Porovná 2 slova a vrací 0 pokud se neliší, zápornou hodnotu pokud první odlišný znak ve "slovo1" < znak na téže pozici ve "slovo2", nebo pokud se znaky neliší, ale "slovo1" je kratší. Kladnou hodnotu funkce vrací ve zbývajících případech</td>
</tr>
<tr>
    <td>int *fieldA = new int[pocetA];</td>
    <td>Vytvoří ukazatel na pole integerů o velikosti "pocetA" prvků</td>
</tr>
<tr>
    <td>vector <double> myVector</td>
    <td>Vektor prvků typu double</td>
</tr>
<tr>
    <td>myVector.push_back(item)</td>
    <td>Přidá nový element na konec vektoru</td>
</tr>
<tr>
    <td>ifstream infile;<br/> infile.open(srcFileName);<br/> infile >> i;<br/> infile.close()</td>
    <td>otevře soubor, uloží int do proměnné i, zavře soubor; #include <fstream></td>
</tr>
<tr>
    <td>ofstream outfile;<br/> outfile.open(dstFileName);<br/> dstfile << i;<br/> outfile.close()</td>
    <td>otevře soubor, uloží do něj proměnnou i, zavře soubor; #include <fstream></td>
</tr>
<tr>
    <td>while(!file.eof())</td>
    <td>Dokud není konec souboru, tak...</td>
</tr>
</table>  
</body>  
</html>