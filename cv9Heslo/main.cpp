#include <iostream>

using namespace std;

int main ()
{
    do{
        string heslo;
        cin >> heslo;
        if(cin.fail()){
            cout << "Heslo nesplnuje pozadavky." << endl;
            cin.clear(); //odstrani chybu a budeme se s ni snazit vyporadat
            cin.ignore(256, '\n'); //ignorujeme vsechny znaky (az 256), nebot jsou chybne, az dokud nedojde k odradkovani
            continue;
            //return 0;
        }

        int test1 = 0;
        int test2 = 0;
        int test3 = 0;

        if(heslo.length() >= 5){
            for(unsigned int i = 0; i < heslo.length(); i++){
                if((heslo[i] >= 'a' && heslo[i] <= 'z') || (heslo[i] >= 'A' && heslo[i] <= 'Z')){
                    test1++;
                }
                else if(heslo[i] >= '0' && heslo[i] <= '9'){
                    test2++;
                }
                else{
                    test3++;
                }
            }
            if(test1 > 0 && test2 > 0 && test3 > 0){
                if((heslo.find('\n') >=0 && heslo.find('\n') <= heslo.length()) ){
                    cout << "Heslo nesplnuje pozadavky." << endl;
                }
                else if((heslo.find(' ') >=0 && heslo.find(' ') <= heslo.length()) ){
                    cout << "Heslo nesplnuje pozadavky." << endl;
                }
                else if((heslo.find('\t') >=0 && heslo.find('\t') <= heslo.length()) ){
                    cout << "Heslo nesplnuje pozadavky." << endl;
                }
                else{
                    cout << "Heslo splnuje pozadavky." << endl;
                }
            }
            else{
                cout << "Heslo nesplnuje pozadavky." << endl;
            }
        }
        else{
             cout << "Heslo nesplnuje pozadavky." << endl;
        }
    }while(cin.peek() != -1);

    return 0;
}
