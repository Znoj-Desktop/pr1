#include <iostream>

using namespace std;

int fact(int n){
    int sum = 1;
    for(int i = 1; i <= n; i++){
        sum *= i;
    }
    return sum;
}
int main()
{
    int n, k;

    cout << "Zadejte n a k:" << endl;
    cin >> n;
    cin >> k;

    if(cin.fail() || n < 0 || k < 0 || n < k){
        cout << "Nespravny vstup." << endl;
        return 0;
    }
    int nFact = fact(n);
    //cout << nFact <<endl;
    int kFact = fact(k);
    //cout << kFact <<endl;;
    int nMinKFact = fact(n-k);
    //cout << nMinKFact <<endl;;

    cout << "C = " << (nFact/(kFact*nMinKFact)) <<endl;
    return 0;
}
