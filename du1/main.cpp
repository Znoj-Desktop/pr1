#include <iostream>
#include <cmath>

#define _USE_MATH_DEFINES

using namespace std;

bool control(double a){
    if(a <= 0 || cin.fail()){
        cout << "Nespravny vstup." << endl;
        return true;
    }
    return false;
}

int main()
{
    char character;
    double a, b;

	do{
    cout << "Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh" << endl;
    cin >> character;
    if(cin.fail()){
        cout << "Nespravny vstup." << endl;
        continue;
    }

    if(character == 'a'){
        cout << "Zadejte stranu ctverce:" << endl;
        cin >> a;
        if(control(a)){
            continue;
            //return 0;
        }

        cout << "Obsah ctverce je: " << a*a <<endl;
        cout << "Obvod ctverce je: " << 4*a <<endl;

    }
    else if(character == 'b'){
        cout << "Zadejte strany obdelniku:" << endl;
        cin >> a >> b;
        if(control(a)){
            continue;
            //return 0;
        }
        else if(control(b)){
            continue;
        }

        cout << "Obsah obdelniku je: " << a*b <<endl;
        cout << "Obvod obdelniku je: " << 2*(a + b) <<endl;

    }
    else if(character == 'c'){
        cout << "Zadejte polomer kruznice:" << endl;
        cin >> a;
        if(control(a)){
            continue;
            //return 0;
        }

        cout << "Obsah kruznice je: " << M_PI*a*a <<endl;
        cout << "Obvod kruznice je: " << 2*M_PI*a <<endl;
    }
    else{
        cout << "Nespravny vstup." << endl;
    }
}while(cin.peek() != -1);

    return 0;
}
