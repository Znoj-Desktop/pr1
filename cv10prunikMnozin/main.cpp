#include <iostream>

using namespace std;

bool isInField(int item, int field[], int length){
    for(int i = 0; i < length; i++){
        if(field[i] == item){
            return true;
        }
    }
    /*
    cout << "last: " << field[length-1] <<endl;
    cout << "index: " <<length - 1 <<endl;
    cout << "next" << field[length] << endl;
    */
    return false;
}

int main()
{
    int pocetA;
    cout << "Zadejte pocet prvku mnoziny A:" << endl;
    cin >> pocetA;
    //int fieldA[255];
    if(cin.fail() || pocetA < 1){
        cout << "Nespravny vstup." << endl;
        return 0;
    }
    int *fieldA = new int[pocetA];

    cout << "Zadejte prvky mnoziny A:" << endl;
    for(int i = 0; i < pocetA; i++){
        cin >> fieldA[i];
        if(cin.fail() || isInField(fieldA[i], fieldA, i)){
            cout << "Nespravny vstup." << endl;
            return 0;
        }
    }

    int pocetB;
    cout << "Zadejte pocet prvku mnoziny B:" << endl;
    cin >> pocetB;
    if(cin.fail() || pocetB < 1){
        cout << "Nespravny vstup." << endl;
        return 0;
    }
    int *fieldB = new int[pocetB];

    cout << "Zadejte prvky mnoziny B:" << endl;
    int pom;
    bool first = true;

    cout << "Prunik mnozin:" << endl;
    cout << "{";
    for(int i = 0; i < pocetB;i++){
        cin >> pom;
        fieldB[i] = pom;
        //cout << "pom: " << pom << endl;
        //cout << "pocetB: " << i << endl;
        if(cin.fail() || isInField(pom, fieldB, i)){
            cout << "Nespravny vstup." << endl;
            return 0;
        }
        if(isInField(pom, fieldA, pocetA)){
            if(!first){
                cout << ", ";
            }
            else{
                first = false;
            }
            cout << pom;
        }
    }

    cout << "}";

    return 0;
}
