#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
    int velikost = 0;
    vector<double> vector1;
    vector<double> vector2;

    cin >> velikost;
    if(cin.fail() || velikost < 1){
        cout << "Nespravny vstup." << endl;
        return 0;
    }

    double pom = 0;
    for(int i = 0; i < velikost; i++){
        cin >> pom;
        if(cin.fail()){
            cout << "Nespravny vstup." << endl;
            return 0;
        }
        vector1.push_back(pom);
    }

    for(int i = 0; i < velikost; i++){
        cin >> pom;
        if(cin.fail()){
            cout << "Nespravny vstup." << endl;
            return 0;
        }
        vector2.push_back(pom);
    }

    double sum = 0;
    double sumA = 0;
    double sumB = 0;
    for(int i = 0; i < velikost; i++){
        sum += (vector1[i]*vector2[i]);
        sumA += (vector1[i]*vector1[i]);
        sumB += (vector2[i]*vector2[i]);
    }
    sumA = sqrt(sumA);
    sumB = sqrt(sumB);
    double s = sumA*sumB;
    cout << "CSM: " << sum /s << endl;
    return 0;
}
