#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int a, b;

    cout << "Zadejte interval:" << endl;

    cin >> a;
    cin >> b;
    if(cin.fail() || b < a){
        cout << "Nespravny vstup." << endl;
        return 0;
    }
    if (a < 1){
        a = 2;
    }
    if(a <= 2){
        cout << 2 << endl;
    }

    int lastPrimary = 2;

    bool prim = true;
    for(int i = a; i <= b; i++){
        for(int k = 2; k < sqrt(i); k++){
            if(i % k == 0){
                //cout << "---" << i << " -> " << k << endl;
                prim = false;
                break;
            }
        }

        if(prim && (i > lastPrimary)){
            cout << i << endl;
            lastPrimary = i;
        }
        prim = true;
    }

    return 0;
}
