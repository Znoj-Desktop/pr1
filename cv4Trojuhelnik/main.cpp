#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    double a, b, c, s, S;
    cout << "Zadejte delky stran trojuhelnika a, b, c:" << endl;


    int k = 25;
    k = floor(k/10);

    if(k < 0){
        cout << "Dluh" << endl;
        return 0;
    }
    switch(k){
        case 4:
            cout << "HDD" << endl;
        case 3:
            cout << "DVD" << endl;
        case 2:
            cout << "CD" << endl;
        case 1:
            cout << "Disketa" << endl;
        case 0:
            cout << "";
            break;
        default:
            cout << "Disketa, CD, DVD, HDD" <<endl;
            break;
    }
    cin >> a >> b >> c;

    s = (a + b + c) / 2;
    double o = s * (s - a) * (s - b) * (s - c);
    S = sqrt(s * (s - a) * (s - b) * (s - c));
    if(s <= 0 || a <= 0 || b <= 0 || c <= 0 || S <= 0 || o <= 0){
        cout << "Nejedna se o platny trojuhelnik." << endl;
        return 0;
    }
    cout << "Obsah trojuhelnika je " << S << endl;
    return 0;
}
