#include <iostream>
#include <math.h>
#include <stdio.h>

using namespace std;


int main()
{
    do{
        int iterations = 0;
        double x = 0;
        double y = 0;
        double fi = 0;
        double delta = 0;
        double v = 0;
        double omega = 0;

        cin >> iterations;
        if(cin.fail() || iterations < 0){
            cout << "Nespravny vstup." <<endl;
            cin.clear(); //odstrani chybu a budeme se s ni snazit vyporadat
            cin.ignore(256, '\n'); //ignorujeme vsechny znaky (az 256), nebot jsou chybne, az dokud nedojde k odradkovani
            continue;
            //return 0;
        }

        cin >> x >> y >> fi >> delta >> v >> omega;
        if(cin.fail()){
            cout << "Nespravny vstup." <<endl;
            cin.clear(); //odstrani chybu a budeme se s ni snazit vyporadat
            cin.ignore(256, '\n'); //ignorujeme vsechny znaky (az 256), nebot jsou chybne, az dokud nedojde k odradkovani
            continue;
            //return 0;
        }

        for(int i = 0; i < iterations; i++){
            x = x + v * delta * cos(fi);
            y = y + v * delta * sin(fi);
            fi = fi + delta * omega;
            while(fi > 2*M_PI){
                fi -= 2*M_PI;
            }
            while(fi < 0){
                fi += 2*M_PI;
            }
            //cout << "x: " << roundf(x*100)/100 << ", y: " << roundf(y*100)/100 << ", fi:" << roundf(fi*100)/100 << endl;
            printf("x: %.2f, y: %.2f, fi: %.2f\n", x, y, fi);
        }
    }while(cin.peek() != -1);
    return 0;
}
