#include <iostream>

using namespace std;

int main()
{
    double vstupniCislo;
    double celkem = 0;
    cout << "Zadavejte cisla, posledni bude 1000:" << endl;

    while(vstupniCislo != 1000){
        cin >> vstupniCislo;
        celkem += vstupniCislo;
    }
    cout << "Vysledek souctu cisel je: " << celkem << endl;
    return 0;
}
