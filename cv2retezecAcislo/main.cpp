#include <iostream>

using namespace std;

int main()
{
  string name;
  int number;

  cout << "Zadej svoje jmeno (bez diakritiky): ";
  getline(cin, name);

  cout << "Zadej cele cislo: ";
  cin >> number;

  cout << "Ahoj, " << name << "." << endl;
  cout << number << " x " << "3 = " << (number * 3) << endl;
  cout << number << " / " << "3 = " << (number / 3) << endl;

  return 0;
}
