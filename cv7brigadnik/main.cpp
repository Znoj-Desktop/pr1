#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int pocetNavstevniku = 0;
    cout << "Zadej pocet navstevniku:" << endl;
    cin >> pocetNavstevniku;
    if((cin.fail()) || (pocetNavstevniku < 0)){
        cout << "Nespravny vstup." << endl;
        return 0;
    }
    double vydelek = floor(pocetNavstevniku*0.07) * 50;
    int plat = 70;

    if(vydelek > 1950){
        plat += 10;
    }

    if(vydelek > 2450){
        plat += 10;
    }

    if(vydelek > 2950){
        plat += 10;
    }

    if(vydelek > 3450){
        plat += 10;
    }

    cout << "Brigadnik vydela: " << plat*8 << " Kc" << endl;
    return 0;
}
