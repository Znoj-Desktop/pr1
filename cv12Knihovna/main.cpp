#include <iostream>

using namespace std;
struct kniha{
    string nazev;
    string prijmeni;
    string jmeno;
    string zanr;
    int rok;
    int cena;
    int id;
};


int main()
{
    const int pocetKnih = 5;
    kniha pole[pocetKnih];
    kniha jednaKniha;
    //do{
        for(int i = 0; i < pocetKnih; i++){
            cout << "Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:" <<endl;
            cin >> jednaKniha.nazev >> jednaKniha.prijmeni >> jednaKniha.jmeno >> jednaKniha.zanr >> jednaKniha.rok >> jednaKniha.cena >> jednaKniha.id;
            pole[i] = jednaKniha;
        }

        cout << "Romany jsou:" <<endl;
        for(int i = 0; i < pocetKnih; i++){
            if(pole[i].zanr == "roman"){
                cout << pole[i].nazev << endl;
            }
        }
        cout << endl;
        cout << "Knihy s cenou mensi nez 300,- Kc jsou:" << endl;
        for(int i = 0; i < pocetKnih; i++){
            if(pole[i].cena < 300){
                cout << pole[i].nazev << endl;
            }
        }
        cout << endl;
        cout << "Prijmeni vsech autoru jsou:" << endl;
        for(int i = 0; i < pocetKnih; i++){
            cout << pole[i].prijmeni << endl;
        }
    //}while(cin.peek() != -1);

    return 0;
}
