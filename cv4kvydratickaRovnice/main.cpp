#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    double a, b, c, d;
    cout << "Zadejte parametry:" << endl;
    cin >> a >> b >> c;

    d = b*b - 4*a*c;
    if(d < 0){
        cout << "Rovnice nema reseni v R.";
        return 0;
    }

    cout << "Koren 1: " << (-b - sqrt(d))/(2*a) << endl;
    cout << "Koren 2: " << (-b + sqrt(d))/(2*a) << endl;
}
