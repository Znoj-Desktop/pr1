#include <iostream>

using namespace std;

int main()
{
    char c;
    long l = 0;
    bool zero = 1;
    cout << "Zadej cislo ve dvojkove soustave:" << endl;
    do{
        c = cin.get();
        if(c == '0'){
            zero = 0;
        }
        else if(c == '1'){
            zero = 0;
            l += 1;
        }
        else if((c == '\n') && zero == 0){
            l/=2;
            break;
        }
        else{
            cout << "Nespravny vstup." << endl;
            return 0;
        }
        l *= 2;

    }while(1);
    cout << "Desitkove cislo je: " << l << endl;

    return 0;
}
