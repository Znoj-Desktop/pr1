#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int pocet;
    cout << "Zadejte pocet schodu:" << endl;
    cin >> pocet;
    if(cin.fail() || pocet <= 0){
        cout << "Nespravny vstup." << endl;
        return 0;
    }

    for(int i = 0; i <= pocet; i++){
        for(int k = 0; k < i; k++){
            if(k%3 == 0)
                cout << "\\";
            if(k%3 == 1)
                cout << "_";
            if(k%3 == 2)
                cout << "/";
        }
        cout << "" << endl;
    }



    for(int i = 0; i <= pocet; i++){
        for(int k = 0; k < i; k++){
            if(k%3 == 0)
                cout << "\\";
            if(k%3 == 1)
                cout << "_";
            if(k%3 == 2)
                cout << "/";
        }
        for(int k = 0; k < 2*(pocet - i); k++){
            cout << " ";
        }

        int z;
        if(i%3 == 1){
            z = 2;
        }
        else if(i%3 == 2){
            z = 1;
        }
        else{
            z = 0;
        }

        for(int k = z; k < (i+z); k++){
            if(k%3 == 0)
                cout << "\\";
            if(k%3 == 1)
                cout << "_";
            if(k%3 == 2)
                cout << "/";
        }
        cout << "" << endl;
    }
    return 0;
}
