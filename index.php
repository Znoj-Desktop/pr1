﻿<!DOCTYPE html>
<html lang="cs">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="keywords" content="Znoj,jiri,Jiří,vsb,fei,soubory,download,informtika,sklad,všb,VŠB,FEI,pr1,programovani,programování,programming,school,1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Jiří Znoj - znoj.cz</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
	<div class="navbar-fixed">
		<nav class="light-blue lighten-1" role="navigation">
			<div class="nav-wrapper container"><a id="logo-container" href="../" class="brand-logo">Jiří Znoj</a>
			  <ul class="right hide-on-med-and-down">
				<li><a href="../#pr1">PR1 (back)</a></li>
				<li><a href="#files">- Referenční řešení</a></li>
				<li><a href="#exercise">- Školní úlohy</a></li>
				<li><a href="#home_exercise">- Domácí úlohy</a></li>
				<li><a href="#functions">- Užitečné funkce</a></li>
				<li><a href="sem.php#semester">- Semestrální práce</a></li>
				
				<li><a href="../#about">About</a></li>
			  </ul>

			  <ul id="nav-mobile" class="side-nav">
				<li><a href="../#pr1">PR1 (←)</a></li>
				<li><a href="#files">- Referenční řešení</a></li>
				<li><a href="#exercise">- Školní úlohy</a></li>
				<li><a href="#home_exercise">- Domácí úlohy</a></li>
				<li><a href="#functions">- Užitečné funkce</a></li>
				<li><a href="sem.php#semester">- Semestrální práce</a></li>
				
				<li><a href="../#about">About</a></li>
			  </ul>
			  <a href="../#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
			</div>
		</nav>
	</div>
	
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<h1 class="header center orange-text">Znoj.cz</h1>
		</div>
	</div>
  	
	<div class="section no-pad-bot" id="pr1">
		<div class="container">
		<br /><br />
			<H4> PR1 </H4>
			<div class='col s12 m12 l12'>
				<div class='card'>
					<div class="card-content black-text">
						<span class="card-title black-text">Email z 5. 12. 2017</span>
						<div>
Dobrý den,
<br /><br />
příští cvičení už budou pouze konzultační. Tedy kdo má obhájen a odevzdán projekt, tak má pro tento semestr hotovo.
<br /><br />
Pokud budete chtít konzultaci nebo obhájit projekt, tak mi napište email a domluvíme se na termínu. <br />
Toto bylo tedy poslední cvičení - neuvědomil jsem si to, tak se omlouvám, že jsem vám to neřekl osobně.
<br /><br />
Kdo bude chtít zápočet, musí (osobně) projekt obhájit a prokázat autorství vlastního kódu. Domluvte si proto se mnou termín pomocí emailu.
<br /><br />
Projekt budete mít opraven (s body) až po úspěšné obhajobě.
<br /><br />
Hodně štěstí u zkoušky, snad se vám cvičení líbila - můžete to vyjádřit  hodnocení předmětu.
<br /><br />
S pozdravem,<br />
Jiří Znoj
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
			<div class='col s12 m12 l12'>
				<div class='card'>
					<div class="card-content black-text">
						<span class="card-title black-text">Windows</span>
						<div>
Otevřte si konzoli ve windows (cmd) a dostaňte se na místo, kde je uložen binární soubor vašeho projektu:<br/><br/>

"cesta k vašemu projektu v Code::Blocks"\bin\Debug\<br/>
<i>nebo "cesta k vašemu projektu v Code::Blocks"\bin\Release\ (záleží, kterou variantu používáte v Code::Blocks při překladu programu)
</i><br/><br/>
spusťte program příkazem "nazevProjektu".exe < input  > output 
						</div>
					</div>
				</div>
			</div>
			<div class='col s12 m12 l12'>
				<div class='card'>
					<div class="card-content black-text">
						<span class="card-title black-text">Linux</span>
						<div>
Otevřte si terminal a dostaňte se na místo, kde je uložen binární soubor vašeho projektu:<br/><br/>

"cesta k vašemu projektu v Code::Blocks"/bin/Debug&ltbr/>
<i>nebo "cesta k vašemu projektu v Code::Blocks"/bin/Release/ (záleží, kterou variantu používáte v Code::Blocks při překladu programu)
</i><br/><br/>
./"nazevProjektu" < input > output 
						</div>
					</div>
				</div>
			</div>
			
			<div class='col s12 m12 l12'>
				<div class='card'>
					<div class="card-content black-text">
						<span class="card-title black-text">Windows + Linux</span>
						<div style="white-space: pre">
- stáhněte si referenční vstupy a výstup z těchto stránek
- porovnejte váš výstup s referenčním výstupem. 
- ve windows lze použít pro porovnání výstupů např. PsPad, v Linuxu příkaz diff
nebo lze využít i online nástroj <a href="https://www.diffchecker.com/diff">https://www.diffchecker.com/diff</a>

Referenční řešení jsou ve tvaru:
referenční vstup: cvN_jmeno_input
referenční výstup: cvN_jmeno_output
kde: <b>N</b> = číslo školní úlohy a <b>jmeno</b> = jméno školní úlohy
						</div>
					</div>
				</div>
			</div>
			<div class='col s12 m12 l12'>
				<div class='card'>
					<div class="card-content black-text">
						<span class="card-title black-text">Úprava programu pro využití souborů *_batch</span>
						<div>
Soubory cvN_jmeno_input obsahují nyní více vstupů. Pokud máte zájem o jejich plné využití, pak si upravte váš program do tvaru:
						<div style="white-space: pre">
<i>int main(){</i>
	<b>do{</b>
		<i>//váš program</i>
		- nahraďte příkazy "return 0" příkazy "<b>continue</b>"
		- pokud používáte "cin.fail()", tak do podmínky (pokud dojde k chybě) uveďte ještě:
		  
		  <b>cin.clear();</b> //odstrani chybu a budeme se s ni snazit vyporadat
		  <b>cin.ignore(256, '\n');</b> //ignorujeme vsechny znaky (az 256), nebot jsou chybne, az dokud nedojde k odradkovani
       
		  jinak se vám vždy po špatném vstupu program ukončí.
	<b>}while(cin.peek() != -1);</b>
<i>}
return 0;</i>
						</div>
Váš výstup pak můžete porovnat se souborem cvN_jmeno_output<b>_batch</b> který obsahuje výstupy pro všechny zvolené vstupy.
Před odevzdáním do progtestu opět výše uvedený cyklus while odstraňte. Pokud máte ve vašem programu "return", pravděpodobně všechny vstupy nezpracují. Pro získání všech výstupů nahraďtě příkazy "return 0" příkazem "continue".
					</div>
					</div>
				</div>
			</div>
			</div>
				
		</div>
	</div>
	
	
	<div class="section no-pad-bot" id="files">
		<div class="container">
			<div class="row">
				<div class='col s12 m6 l6'>
					<div class='card center'>
						<a href="https://www.pracujprosiliconvalley.cz/studenti-a-absolventi/?utm_source=web&utm_campaign=bakalar&utm_medium=banner&utm_content=hiring_znoj" target="_blank"><img src="../468x60-bakal.jpg"></img></a>
					</div>
				</div>
				<div class='col s12 m6 l6'>
					<div class='card center'>
						<a href="https://www.pracujprosiliconvalley.cz/pozice/?utm_source=web&utm_campaign=pvsv&utm_medium=banner&utm_content=hiring_znoj" target="_blank"><img src="../468x60-hiring.jpg"></img></a>
					</div>
				</div>
			</div>
			<H4> Referenční řešení </H4>
	 
			<div class="row">
    
			  <?php
				// Opens directory
				$myDirectory=opendir("./download");
				
				// Gets each entry
				while($entryName=readdir($myDirectory)) {
				  $dirArray[]=$entryName;
				}
				
				
				// Closes directory
				closedir($myDirectory);
				
				// Counts elements in array
				$indexCount=count($dirArray);
				// Sorts files
				sort($dirArray);
				
				// Loops through the array of files
				for($index=0; $index < $indexCount; $index++) {
				  
				  // Gets File Names
				  $name=$dirArray[$index];
				  if($name == '.' || $name == '..'){
					  continue;
				  }
				  $namehref=$dirArray[$index];
				  
				  
				  // Print 'em
				  print("
						<div class='col s12 m6 l3'>
						<div class='card'>
							<a href='./download/$namehref'>
									<div class='card-image waves-effect waves-block waves-light'>
									</div>
									<div class='card-content'>
										<p class='card-text activator grey-text text-darken-4'>$name</p>
									</div>
								</div>
							</a>
						</div>
				  ");
				}
			  ?>
			</div>
		</div>
	</div>

    <div class="section no-pad-bot" id="exercise">
		<div class="container">
			<br /><br />
			<H4> Školní úlohy </H4>
				<ul id="dropdown2" class="dropdown-content light-blue lighten-1">
					<li><a href="#1" class="white-text">1</a></li>
					<li><a href="#2" class="white-text">2</a></li>
					<li><a href="#3" class="white-text">3</a></li>
					<li><a href="#4" class="white-text">4</a></li>
					<li><a href="#5" class="white-text">5</a></li>
					<li><a href="#6" class="white-text">6</a></li>
					<li><a href="#7" class="white-text">7</a></li>
					<li><a href="#8" class="white-text">8</a></li>
					<li><a href="#9" class="white-text">9</li>
					<li><a href="#10" class="white-text">10</a></li>
					<li><a href="#11" class="white-text">11</a></li>
					<li><a href="#12" class="white-text">12<span class="new red badge"></span></a></li>
				</ul>
				<a class="btn dropdown-button light-blue lighten-1 grey-text text-lighten-5" href="#!" data-activates="dropdown2">Týden<i class="mdi-navigation-arrow-drop-down right"></i></a>
			<div class="row" id=1>
			<br /><br />
			<H5> 1 - Hello World </H5>
				Realizujte program, který vypíše na obrazovku text "Hello world!" a odřádkování. Text bude zobrazen bez uvozovek.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázce. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí. Možná ve svém programu používáte volání:
				<br /><br />
				<div style="white-space: pre">
#include 

using namespace std;

int main(){
  ...				  
  return 0;
}
				</div>
			</div>
			
			<div class="row" id=2>
			<br /><br />
			<H5>2 - Dvě čísla </H5>
				Vytvořte program, který načte dvě čísla a zobrazí výsledek jejich součtu, rozdílu, součinu a podílu. Při komunikaci s uživatelem a formátování výstupu se řiďte ukázkovými příklady níže. Neřešte možné chyby na vstupu.
				<br /><br />
				Vstupem programu jsou dvě čísla.
				<br /><br />
				Výstupem programu jsou načtená čísla a komentované výsledky hodnot základních aritmetických operací nad nimi.
				<br /><br />
				Výstup čísel nijak neupravujte, mezi čísla, operátory a znaménka vkládejte mezery, za posledním výstupem odřádkujte, jak je běžné a jak vidíte na výpisu - jeho formátování musí být dodrženo.
				<br /><br />
				Pro číselné proměnné používejte typ double. Nepoužívejte žádné funkce. Kostra Vašeho programu bude vypadat následovně:
				<div style="white-space: pre">
int main()
{
  double a, b;
 
  return 0;
}
				</div>
			</div>

			<div class="row">
			<H5>2 - Řetězec a číslo </H5>
				Vytvořte program, který načte řetězec a číslo. Program vypíše načtený řetězec v daném tvaru a provede násobení a dělení zadaného čísla konstantou. Výpočty pak vypíše také. Při komunikaci s uživatelem a formátování výstupu se řiďte ukázkovými příklady níže (řetězec může obsahovat více slov). Neřešte možné chyby na vstupu.
				<br /><br />
				Vstupem programu je řetezec a celé číslo.
				<br /><br />
				Výstupem programu je řetězec obsahující načtený řetězec na jednom řádku, na druhém násobení načteného čísla konstantou a na třetím vydělení načteného stejnou konstantou.
				<br /><br />
				Zmiňovanou konstantou je číslo 3. Nepoužívejte žádné vlastní funkce. Kostra Vašeho programu bude vypadat takto:	
				<div style="white-space: pre">
int main()
{
  string name;
  int number;
  
  return 0;
}
				</div>
			</div>
			
			<div class="row" id=3>
				<br /><br />
			<H5>3 - Kružnice </H5>
				Vytvořte program, který vypíše informace pro sestaveni rovnice kružnice, jejímž průměrem je úsečka AB.
				<br /><br />
				Vstupem programu je dvojice bodů A a B s x-ovou a y-ovou souřadnicí: A[x,y], B[x,y].
				<br /><br />
				Výstupem programu jsou následující informace v pořadí dle ukázek:
				<br /><br />
				- X-ova souřadnice středu S: (Ax + Bx) / 2
				<br /> - Y-ova souřadnice středu S: (Ay + By) / 2
				<br /> - Délka směrového vektoru u: sqrt((Bx - Ax) * (Bx - Ax) + (By - Ay) * (By - Ay))
				<br /> - Poloměr r kružnice: polovina délky směrového vektoru
				<br /><br />
				<b>Nápověda:</b>
				<br /> - Pro uložení desetinných čísel používejte datový typ double, nepoužívejte float.
				<br /> - Druhá mocnina čísla x se vypočítá jako x * x.
				<br /> - Pro odmocninu použijte funkci sqrt z matematické knihovny (#include <cmath> - viz příklad níže).
				<br /> - Neočekávejte neplatné vstupy - na vstupu budou vždy čísla.
				<br /> - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy (viz níže) vypadat přesně tak, jako ukázky.
				<br /> - Na konci výpisu je prázdný řádek.
				<br />Příklad použití funkce sqrt:
				<div style="white-space: pre">
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	double number = 25, result;
	result = sqrt(number);	// result ma ted hodnotu 5	
	
	return 0;
}			
				</div>
			</div>

			<div class="row">
				<br /><br />
			<H5>3 - Tři čísla</H5>
				Realizujte program, který nad třemi čísly a, b, c provede součet, součin a rozdíl. Rozdíl bude počítán takto: a - b - c.
				<br /><br />
				Vstupem programu jsou 3 celá čísla.
				<br /><br />
				Výstupem programu je součet čísel, součin a rozdíl. V tomto pořadí.
			</div>
			
			<div class="row" id=4>
				<br /><br />
			<H5>4 - Kvadratická rovnice</H5>
				Vytvořte program, který ze zadaných parametrů a, b a c vypočítá kořeny kvadratické rovnice.
				<br /><br />
				Vstupem programu jsou tři desetinná čísla reprezentující parametry kvadratické rovnice a, b a c.
				<br /><br />
				Výstupem programu jsou kořeny kvadratické rovnice v pořadí a tvaru dle ukázek.
				<br /><br />
				<b>Nápověda:</b>
				<br /> - Pro uložení desetinných čísel používejte datový typ double, nepoužívejte float.
				<br /> - Neočekávejte neplatné vstupy - na vstupu budou vždy reálná čísla.
				<br /> - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy (viz níže) vypadat přesně tak, jako ukázky.
				<br /> - Na konci výpisu je prázdný řádek.
				<br /><br />
			</div>

			<div class="row">
			<H5>4 - Trojúhelník </H5>
				Vytvořte program, který načte tři reálná čísla a rozhodne, zda tato čísla představují délky stran skutečného trojúhelníka. Pokud takový trojúhelník existuje, program vypočítá jeho obsah. Pokud ne, program to řekne.
				<br /><br />
				Vstupem programu jsou tři reálná čísla.
				<br /><br />
				Výstupem programu je obsah trojúhelníka. Pokud trojúhelník neexistuje, je výstupem informace, že trojúhelník neexistuje.
				<br /><br />
				<b>Nápověda:</b>
				<br /> - Obsah S trojúhelníka se stranami a, b, c se vypočítá podle Heronova vzorce: S = sqrt(s * (s - a) * (s - b) * (s - c)), kde s = (a + b + c) / 2.
				<br /> - Trojúhelník neexistuje, pokud je jeho obsah záporný.
				<br /> - Neočekávejte neplatné vstupy - na vstupu budou vždy reálná čísla.
				<br /> - Pozor na mezery, odřádkování, písmena. Až budete svůj program zkoušet ve vývojovém prostředí, textový výstup programu musí pro ukázkové vstupy (viz níže) vypadat přesně tak, jako ukázky.
				<br /> - Na konci výpisu je prázdný řádek.
			</div>

			<div class="row" id=5>
				<br /><br />
			<H5>5 - Schody </H5>
				Vytvořte program, který pro zadaný počet schodů vykreslí jejich obraz podle vzoru na ukázkách níže.
				<br /><br />
				Vstupem programu je jedno číslo - počet schodů.
				<br /><br />
				Výstupem programu je tolik řádků, kolik odpovídá počtu schodů, schod je tvořen znakem '_' (podtržítko), výplňové znaky zleva znakem 'X', výstup je na ukázkách níže.
				<br /><br />
				Pozor, na vstupu se mohou vyskytnout nedovolené číselné hodnoty i hodnoty jiných datových typů (nečíselné hodnoty). Možné chyby na vstupu musíte ošetřit.
				<br /><br />
			</div>

			<div class="row">
			<H5>5 - Validace vstupu </H5>
				Tímto programem prokážete zvládnutí ošetření chybného vstupu. Vytvořte program, který má za úkol načíst desetinné číslo a zobrazit jej, nebo ohlásit chybu. Podrobněji vidíte chování programu na ukázkách níže.
				<br /><br />
				Vstupem programu je jedno desetinné číslo.
				<br /><br />
				Výstupem programu je zadané číslo (nebo jeho část) s informací, že vstup je v pořádku, případně (pokud bude zadáno něco jiného) ohlásí program nesprávný vstup. Formát výstupu je v ukázkách níže. Pro zvládnutí povinného testu stačí správně ošetřit jednodušší případ, tedy ohlásit úspěch i při načtení části čísla. Tedy jednodušší řešení může pro vstup 123.45.67 oznámit úspěch (načte 123.45).
				<br /><br />
				Pokud Váš program dokáže správně odmítnout i takto špatně zapsaná čísla, uspěje i v druhém nepovinném testu a získá malý bonus.
				<br /><br />
				Pozor, na vstupu se mohou vyskytnout jak číselné hodnoty nebo jejich části, tak hodnoty jiných datových typů (nečíselné hodnoty). Možné chyby na vstupu musíte ošetřit a zapsat výstup podle vzoru, jinou funkčnost program nemá.
			</div>
			
			<div class="row" id=6>
				<br /><br />
			<H5>6 - Binární číslo </H5>
				Napište program, který převádí celá čísla zapsaná ve dvojkové soustavě na čísla v soustavě desítkové, která zapíše do výstupu.
				<br /><br />
				Vstupem programu je posloupnost znaků 0 a 1, představující normální zápis dvojkového čísla, tj. zapsané zleva doprava, nejvyšší řád je vlevo, nejnižší vpravo. Vstup je ukončen koncem řádku. Každý řádek vstupu obsahuje právě jedno dvojkové číslo. Pokud jsou v řetězci jiné znaky, ohlásí program chybu a ukončí se.
				<br /><br />
				Výstupem programu je desítková celočíselná hodnota přečteného binárního čísla, nebo hlášení o chybném vstupu. Na konci výstupu je vždy nový řádek.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše řešení považované za nesprávné. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (tedy i za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
			</div>

			<div class="row">
			<H5>6 - While cyklus </H5>
				Vytvořte program, který sčítá reálná čísla na vstupu, dokud nepřečte hodnotu 1000. Tato hodnota se rovněž přičte k celkovému součtu, který se zobrazí a program skončí, viz ukázka výstupu níže.
				<br /><br />
				Vstupem programu jsou reálná čísla, posledním číslem je 1000.
				<br /><br />
				Výstupem programu je součet zadaných reálných čísel následovaný novým řádkem.
				<br /><br />
				Na vstupu jsou pouze číselné hodnoty, poslední je 1000; netestujte možné chyby vstupu.
				<br /><br />
				Dodržte přesně formát všech výpisů podle ukázky. Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
			</div>

			<div class="row" id=7>
				<br /><br />
			<H5>7 - Brigádníkův plat </H5>
				Hrad má určitou návštěvnost. Na hradu je pro návštěvníky k dispozici atrakce placená 50 korunami. 7% návštěvníků hradu tuto atrakci využije a zaplatí. Atrakci obsluhuje brigádník, jehož plat je dán denním obratem na atrakci:
				<br /><br />
				0 - 1950 Kč.......7O Kč/h, <br />
				2000 - 2450 Kč....80 Kč/h, <br />
				2500 - 2950 Kč....90 Kč/h, <br />
				3000 - 3450 Kč....100 Kč/h, <br />
				3500 a více Kč....110 Kč/h.
				<br /><br />
				Brigádník pracuje 8 hodin denně. Napište program, který pro zadaný počet návštěvníků hradu vypočte kolik si brigádník vydělá.
				<br /><br />
				Vstupem programu je přirozené číslo, představující počet návštěvníků hradu.
				<br /><br />
				Výstupem programu je odhadovaný plat brigádníka. Je-li zapotřebí zaokrouhlování, zaokrouhlujte vždy dolů.
				<br /><br />
				Program kontroluje správnost vstupů. Požadavky na správnost vstupu z hlediska počtu zákazníků jsou zřejmé. Na libovolné neshody s požadavky na správnost program zareaguje výpisem řetězce "Nesprávný vstup." a ukončením.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadej pocet navstevniku:
1000
Brigadnik vydela: 880 Kc

Zadej pocet navstevniku:
100
Brigadnik vydela: 560 Kc

Zadej pocet navstevniku:
523
Brigadnik vydela: 560 Kc

Zadej pocet navstevniku:
1234
Brigadnik vydela: 880 Kc

Zadej pocet navstevniku:
-5
Nespravny vstup.

Zadej pocet navstevniku:
0
Brigadnik vydela: 560 Kc

Zadej pocet navstevniku:
Vyhodili jsme brigadnika.
Nespravny vstup.
				</div>
			</div>

			<div class="row">
			<H5>7 - Hexadecimální číslo </H5>
				Realizujte program, který převede hexadecimální číslo na decimální.
				<br /><br />
				Vstupem programu je jednoslovný řetězec představující hexadecimální číslo. Písmena hexadecimálního zápisu mohou být velká i malá. Bezprostředně za vlastním číslem je odřádkování.
				<br /><br />
				Výstupem programu je decimální hodnota vstupního čísla, ukončená znakem pro odřádkování.
				<br /><br />
				Pokud je vstupní řetězec nesprávný (obsahuje neplatné cifry, nebo za číslem nenásleduje odřádkování, program vypíše chybové hlášení podle ukázky a ukončí se.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.								
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte hexadecimalni cislo:
abeced
Desitkove: 11267309

Zadejte hexadecimalni cislo:
alpha
Nespravny vstup.

Zadejte hexadecimalni cislo:
AAAFF
Desitkove: 699135

Zadejte hexadecimalni cislo:
ostrava
Nespravny vstup.

Zadejte hexadecimalni cislo:
abc 12
Nespravny vstup.
				</div>
			</div>
			
			<div class="row" id=8>
				<br /><br />
			<H5>8 - Kombinační čísla</H5>
				Realizujte program, který spočítá kombinační číslo.
				<br /><br />
				Vstupem programu jsou dvě celá čísla, n a k, oddělená mezerou.
				<br /><br />
				Výstupem programu je hodnota kombinačního čísla n nad k vypočítaná podle vzorce n! / (k! * (n - k)!). Na konci výstupu je vždy prázdný řádek.
				<br /><br />
				Program kontroluje vstupy dle platných pravidel pro výpočet kombinačního čísla.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte n a k:
4 2
C = 6

Zadejte n a k:
12 5
C = 792

Zadejte n a k:
0 0
C = 1

Zadejte n a k:
3 -2
Nespravny vstup.

Zadejte n a k:
1 6
Nespravny vstup.

Zadejte n a k:
hello world
Nespravny vstup.
				</div>
			</div>

			<div class="row">
			<H5>8 - Prvočísla</H5>
				Napište program, který vypíše všechna prvočísla ze zvoleného rozsahu.
				<br /><br />
				Na vstupu jsou dvě celá kladná čísla udávající začátek a konec intervalu (včetně).
				<br /><br />
				Na výstupu je seznam nalezených prvočísel, každé na novém řádku.
				<br /><br />
				Program musí kontrolovat správnost vstupu. Pokud je vstup nesprávný, zobrazte chybovou hlášku a ukončete program. Za chybu považujte:
				<br />
				 - nečíselné vstupy,<br />
				 - dolní mez vyšší než horní mez.<br /><br />
				Uvažujte pouze meze menší než 2000000000, tedy pro ukládání hodnot použijte datový typ int.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte interval:
0 10
2
3
5
7

Zadejte interval:
-200 15
2
3
5
7
11
13

Zadejte interval:
3 17
3
5
7
11
13
17

Zadejte interval:
1999999900 2000000000
1999999913
1999999927
1999999943
1999999973

Zadejte interval:
13 13
13

Zadejte interval:
1 test
Nespravny vstup.

Zadejte interval:
test 2
Nespravny vstup.

Zadejte interval:
30 20
Nespravny vstup.
				</div>
			</div>	
			
			<div class="row" id=9>
				<br /><br />
			<H5>9 - Heslo</H5>
				Realizujte program, který pro zadané heslo zjistí, zda splňuje následující podmínky:
				<br />
				<br /> - musí obsahovat nejméně 5 znaků,
				<br /> - musí obsahovat alespoň jedno malé nebo velké písmeno,
				<br /> - musí obsahovat alespoň jednu číslici,
				<br /> - musí obsahovat alespoň jeden znak, který není číslicí nebo písmenem.
				<br /><br />
				Vstupem programu je jednoslovný řetězec (řetězec neobsahující mezery, konce řádků ani jiné bílé znaky).
				<br /><br />
				Výstupem programu je informace o tom, zda vstupní řetězec splňuje podmínky. Na konci výstupu je odřádkování.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
abcdefgh123451#$@@#!
Heslo splnuje pozadavky.

1a2a3a%a^
Heslo splnuje pozadavky.

abcedfed
Heslo nesplnuje pozadavky.

a1@#
Heslo nesplnuje pozadavky.

abce212
Heslo nesplnuje pozadavky.
				</div>
			</div>

			<div class="row">
			<H5>9 - Porovnávání řetězců</H5>
				Realizujte program, který pro tři slova vypíše počet znaků, které obsahují, a porovná každé s každým. Vypíše, zda se slova rovnají či nikoliv.
<br /><br />
Vstupem programu jsou tři řetězce slovo1, slovo2 a slovo3.
<br /><br />
Výstupem programu jsou následující informace v pořadí dle ukázek:
<br />
<br/ > - informace, zda se shoduje slovo1 a slovo2
<br/ > - informace, zda se shoduje slovo1 a slovo3
<br/ > - informace, zda se shoduje slovo2 a slovo3
<br/ > - počet znaků ve slovo1
<br/ > - počet znaků ve slovo2
<br/ > - počet znaků ve slovo3 <br /><br />
Program detekuje chybu, pokud na vstupu nejsou zadaná požadovaná tři slova. V takovém případě vypíše chybové hlášení dle ukázky a ukončí se. Chybové hlášení vypisujte na standardní výstup (nevypisujte jej na standardní chybový výstup).
<br /><br />
Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
<br /><br />
Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte 3 slova:
ahoj moje prikladzevsechnejtezsi
Slovo1 a slovo2 nejsou stejna.
Slovo1 a slovo3 nejsou stejna.
Slovo2 a slovo3 nejsou stejna.
Pocet znaku ve slovo1 je: 4
Pocet znaku ve slovo2 je: 4
Pocet znaku ve slovo3 je: 22

Zadejte 3 slova:
cviceni java programovani
Slovo1 a slovo2 nejsou stejna.
Slovo1 a slovo3 nejsou stejna.
Slovo2 a slovo3 nejsou stejna.
Pocet znaku ve slovo1 je: 7
Pocet znaku ve slovo2 je: 4
Pocet znaku ve slovo3 je: 12

Zadejte 3 slova:
hello world
Nespravny vstup.

Zadejte 3 slova:
jednodenni ale ale
Slovo1 a slovo2 nejsou stejna.
Slovo1 a slovo3 nejsou stejna.
Slovo2 a slovo3 jsou stejna.
Pocet znaku ve slovo1 je: 10
Pocet znaku ve slovo2 je: 3
Pocet znaku ve slovo3 je: 3

Zadejte 3 slova:
dva akvarium dva
Slovo1 a slovo2 nejsou stejna.
Slovo1 a slovo3 jsou stejna.
Slovo2 a slovo3 nejsou stejna.
Pocet znaku ve slovo1 je: 3
Pocet znaku ve slovo2 je: 8
Pocet znaku ve slovo3 je: 3

Zadejte 3 slova:
malo malo malo
Slovo1 a slovo2 jsou stejna.
Slovo1 a slovo3 jsou stejna.
Slovo2 a slovo3 jsou stejna.
Pocet znaku ve slovo1 je: 4
Pocet znaku ve slovo2 je: 4
Pocet znaku ve slovo3 je: 4

Zadejte 3 slova:
dvatisicedvestedvacetdva dvatisicedvestedvacetdvaapul trista
Slovo1 a slovo2 nejsou stejna.
Slovo1 a slovo3 nejsou stejna.
Slovo2 a slovo3 nejsou stejna.
Pocet znaku ve slovo1 je: 24
Pocet znaku ve slovo2 je: 28
Pocet znaku ve slovo3 je: 6

Zadejte 3 slova:
NEMAMRAD nemamrad NEmamRAD
Slovo1 a slovo2 jsou stejna.
Slovo1 a slovo3 jsou stejna.
Slovo2 a slovo3 jsou stejna.
Pocet znaku ve slovo1 je: 8
Pocet znaku ve slovo2 je: 8
Pocet znaku ve slovo3 je: 8
				</div>
			</div>	
			
			<div class="row" id=10>
				<br /><br />
			<H5>10 - Kosinová míra podobnosti </H5>
				Realizujte program, který pro dva zadané vektory spočítá jejich podobnost pomocí kosinové podobnosti (CSM - Cosine Similarity Measure).
				<br /><br />
				Vstupem programu je na prvním řádku celé číslo N udávající počet prvků obou vektorů. Na druhém řádku se nachází N desetinných čísel udávajících hodnoty prvního vektoru, na třetím řádku pak N desetinných čísel udávajících hodnoty druhého vektoru.
				<br /><br />
				Výstupem programu je kosinus úhlu mezi vstupními vektory. Výpočet kosinové podobnosti je snadný - označíme-li si vektory v1 a v2, je podobnost dána vztahem 
				<br />(v1 * v2) / (||v1|| * ||v2||). <br />
				Na konci výstupu je vždy odřádkování.
				<br /><br />
				Program kontroluje správnost vstupů. Počet prvků vektoru nesmí být záporný ani nula, ostatní parametry mohou nabývat libovolné hodnoty desetinného čísla. Je-li vstup chybný, vypíše program řetězec "Nesprávný vstup." a ukončí se.
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
5
12 15 0 1 5
0.12 0.14 1.54 2 14
CSM: 0.267

5
0 0 0 1 2
0.1 0 0 0 1
CSM: 0.890

12
0 1 2 3 4 5 6 7 8 9 10 11
11 10 9 8 7 6 5 4 3 2 1 0
CSM: 0.435

5
5.12 9.21 9.68 14.54 0.1
5.43 6.43 6.57 540 test
Nespravny vstup.

-5
Nespravny vstup.
				</div>
			</div>

			<div class="row">
			<H5>10 - Průnik množin </H5>
				Realizujte program, který pro dvě množiny určí jejich PRŮNIK.
				<br /><br />
				Vstupem programu je počet prvků množiny a prvky množiny (celá čísla).
				<br /><br />
				Výstupem programu jsou vypsané prvky průniku zadaných dvou množin. Pořadí prvků ve výpisu není podstatné, prvky se ale ve výpisu nesmí opakovat (jsou to množiny).
				<br /><br />
				Program detekuje chybu, zobrazí chybové hlášení dle ukázky a ukončí se, pokud jsou na vstupu nečíselné hodnoty nebo pokud nějaká vstupní množina obsahuje stejný prvek vícekrát (jsou to množiny!). Chybové hlášení vypisujte na standardní výstup (nevypisujte jej na standardní chybový výstup).
				<br /><br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte pocet prvku mnoziny A:
5
Zadejte prvky mnoziny A:
1 3 7 9 4
Zadejte pocet prvku mnoziny B:
6
Zadejte prvky mnoziny B:
15 1 4 99 3 17
Prunik mnozin:
{1, 3, 4}

Zadejte pocet prvku mnoziny A:
7
Zadejte prvky mnoziny A:
1 2 3 4 5 6 7
Zadejte pocet prvku mnoziny B:
7
Zadejte prvky mnoziny B:
6 4 5 2 3 1 7
Prunik mnozin:
{1, 2, 3, 4, 5, 6, 7}

Zadejte pocet prvku mnoziny A:
4
Zadejte prvky mnoziny A:
10 8 27 16
Zadejte pocet prvku mnoziny B:
2
Zadejte prvky mnoziny B:
33 17
Prunik mnozin:
{}

Zadejte pocet prvku mnoziny A:
5
Zadejte prvky mnoziny A:
1 8 12 6 8
Nespravny vstup.

Zadejte pocet prvku mnoziny A:
-3
Nespravny vstup.

Zadejte pocet prvku mnoziny A:
2
Zadejte prvky mnoziny A:
5 abcd
Nespravny vstup.
				</div>
			</div>
			
			<div class="row" id=11>
				<br /><br />
			<H5>11 - Lokalizace robota </H5>
				Realizujte program, který simuluje pohyb robota výpočtem souřadnic, na kterých se robot bude v každém kroku nacházet, a úhlu, v jakém robot bude natočen.
				<br /><br />
				Vstupem programu je několik parametrů. Prvním z nich je kladné celé číslo na prvním řádku označující počet iterací, kolikrát se provede výpočet souřadnic a úhlu. Na druhém řádku je pak 6 desetinných čísel udávající v tomto pořadí následující parametry: počáteční souřadnice x, počáteční souřadnice y, počáteční natočení (úhel) robota fi, časový rozdíl mezi jednotlivými kroky výpočtu delta, rychlost pohybu v a úhel, o který se robot natočí po jednom kroku omega.
				<br /><br />
				Výstupem programu jsou informace o tom, na jakých souřadnicích a s jakým natočením se robot nachází po každém kroku. Formát výstupu viz. ukázky. Vzorce pro výpočet nových parametrů v každém kroku jsou tyto: <br />
				x' = x + v * delta * cos(fi), <br />
				y' = y + v * delta * sin(fi), <br />
				fi' = fi + delta * omega. <br />
				Parametry s čárkou označují nové hodnoty. Po skončení každého kroku (iterace) se nové hodnoty stávají hodnotami výchozími pro další výpočet (tj. hodnoty bez čárky). Vyjde-li úhel fi' mimo interval 0 až 2*π, přičtěte/odečtěte periodu 2*π tak, aby se do tohoto intervalu vešel. Pracujte s konstantou M_PI z matematické knihovny. Za každým zobrazeným řádkem je znak odřádkování (\n).
				<br /><br />
				Program kontroluje správnost vstupů. Počet iterací nesmí být záporný ani nula, ostatní parametry mohou nabývat libovolné hodnoty desetinného čísla. Je-li vstup chybný, vypíše program řetězec "Nesprávný vstup." a ukončí se.
				<br /><br />
Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
5
12 15 0 1 5 0.12
x: 17.00, y: 15.00, fi: 0.12
x: 21.96, y: 15.60, fi: 0.24
x: 26.82, y: 16.79, fi: 0.36
x: 31.50, y: 18.55, fi: 0.48
x: 35.94, y: 20.86, fi: 0.60

10
0 0 0 1 2 0.1
x: 2.00, y: 0.00, fi: 0.10
x: 3.99, y: 0.20, fi: 0.20
x: 5.95, y: 0.60, fi: 0.30
x: 7.86, y: 1.19, fi: 0.40
x: 9.70, y: 1.97, fi: 0.50
x: 11.46, y: 2.93, fi: 0.60
x: 13.11, y: 4.06, fi: 0.70
x: 14.64, y: 5.34, fi: 0.80
x: 16.03, y: 6.78, fi: 0.90
x: 17.28, y: 8.34, fi: 1.00

-5
Nespravny vstup.

5
error
Nespravny vstup.
				</div>
			</div>

			<div class="row">
			<H5>11 - Soubory s čísly </H5>
				Realizujte funkci (ne celý program, pouze jednu funkci), která načte čísla ze zadaného souboru, vytvoří nový soubor a do vytvořeného nového souboru zapíše nejprve čísla sudá a za ně čísla lichá.
				<br /><br />
				Funkce bude mít rozhraní:
				<br /><br />
				<b>int evenOdd ( const char * srcFileName, const char * dstFileName );</b>
				<br /><br />
				<b>srcFileName</b><br />
				&emsp;je řetězec se jménem zdrojového souboru. Soubor obsahuje čísla zapsaná v desítkové podobě, na řádce je vždy jedno číslo.
				<br /><br />
				<b>dstFileName</b><br />
				&emsp;je řetězec se jménem cílového souboru. Funkce do tohoto souboru zapíše čísla čtená ze zdrojového souboru, ale nejprve čísla sudá (zachová pořadí ve kterém byla ve zdrojovém souboru) a za ně pak čísla lichá (opět zachová pořadí ve zdrojovém souboru).
				<br /><br />
				<b>návratová hodnota</b><br />
				&emsp;funkce vrací hodnotu 1 pro úspěch (soubory se podařilo zpracovat) nebo 0 (chyba při práci se soubory).
				<br /><br />
				Dodržte přesně rozhraní funkce evenOdd. Do odevzdávaného zdrojového souboru vložte Vaší implementovanou funkci evenOdd a případné další Vaše podpůrné funkce, které pro správný chod evenOdd potřebujete. Vkládání hlavičkových souborů a funkci main ponechte v bloku podmíněného překladu, jak je ukázáno v následující ukázce. Doporučujeme zkopírovat si šablonu do Vašeho projektu a doplnit pouze požadované implementace funkcí.
				<div style="white-space: pre">
#ifndef __PROGTEST__
#include <iostream>
#include <cstdlib>
#endif /* __PROGTEST__ */

/* Vase pomocne funkce (jsou-li potreba) */

int evenOdd ( const char * srcFileName, const char * dstFileName )
 { 
   /* implementace */
 }

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
 {
   /* Vase testy */
   return 0;
 }
#endif /* __PROGTEST__ */
				</div>
				<br /><br />
				<b>Ukázka použití funkce:</b>
				<div style="white-space: pre">
/* file in.txt:
------8<-----
3
6
7
2
0
20
19
0
-3
451
------8<-----
*/
evenOdd ( "in.txt", "out.txt" ); /* return: 1 */
/* file out.txt:
------8<-----
6
2
0
20
0
3
7
19
-3
451
------8<-----
*/

/* file in.txt:
------8<-----
2
6
-4
------8<-----
*/
evenOdd ( "in.txt", "out.txt" ); /* return: 1 */
/* file out.txt:
------8<-----
2
6
-4
------8<-----
*/

/* file in.txt:
------8<-----
7
hello
------8<-----
*/
evenOdd ( "in.txt", "out.txt" ); /* return: 0 */
				</div>
			</div>	
			
			<div class="row" id=12>
				<br /><br />
			<H5>12 - Struktury - knihovna</H5>
				Realizujte program, který bude simulovat mini knihovnu o pěti knihách :).
				<br /><br />
				Vstupem programu je název knihy, příjmení autora, jméno autora, žánr, rok vydání, cena a id. Název knihy, příjmení a jméno autora a žánr knihy reprezentujte jako řetězce (délka 29 znaků postačuje). Rok vydání, cenu a id reprezentujte jako celá čísla. Informace o jedné knize seskupte do struktury.
				<br /><br />
				Výstupem programu jsou následující informace v pořadí dle ukázek:
				<br /><br />
				- vypíše názvy všech románů,<br />
				- vypíše názvy všech knih, které mají cenu menší než 300,- Kč,<br />
				- vypíše příjmení všech autorů.<br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém).
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
________________________________________

Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Broucci Karafiat Jan pohadky 1975 150 121213
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Anatom Andahazi Federico roman 1999 250 152698
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Sedmero_pohadek Paustovskij K.G. pohadky 1974 40 235987
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Cinska_nevesta Putney M.J. roman 2001 110 25975
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Hrisnik Hunter M. roman 2008 120 45456487
Romany jsou:
Anatom
Cinska_nevesta
Hrisnik

Knihy s cenou mensi nez 300,- Kc jsou:
Broucci
Anatom
Sedmero_pohadek
Cinska_nevesta
Hrisnik

Prijmeni vsech autoru jsou:
Karafiat
Andahazi
Paustovskij
Putney
Hunter
________________________________________

Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Fontana_pro_Zuzanu Gasparova E. roman 1975 30 2555959
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Svudkyne Johnson S. roman 2009 130 2598742
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Indocinou_na_kole Cerveny A. cestopis 2004 99 6589456
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Tanec_s_draky Martin G.R.R roman 2012 350 11111111
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Vratne_lahve Sverak Z. povidka 2007 70 22222
Romany jsou:
Fontana_pro_Zuzanu
Svudkyne
Tanec_s_draky

Knihy s cenou mensi nez 300,- Kc jsou:
Fontana_pro_Zuzanu
Svudkyne
Indocinou_na_kole
Vratne_lahve

Prijmeni vsech autoru jsou:
Gasparova
Johnson
Cerveny
Martin
Sverak
________________________________________

Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Laska_v_nebezpeci Quinn J. roman 2011 69 26548
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Cesta_kolem_sveta_za_80_dni Verne J. roman 1971 150 66585
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Hra_o_truny Martin G.R.R sci-fi 2011 160 789878
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
1984  Orwel G. sci-fi 2009 220 121212
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
451_stupnu_Fahrenheita Bradbury R.D. sci-fi 1957 150 363938
Romany jsou:
Laska_v_nebezpeci
Cesta_kolem_sveta_za_80_dni

Knihy s cenou mensi nez 300,- Kc jsou:
Laska_v_nebezpeci
Cesta_kolem_sveta_za_80_dni
Hra_o_truny
1984
451_stupnu_Fahrenheita

Prijmeni vsech autoru jsou:
Quinn
Verne
Martin
Orwel
Bradbury
________________________________________

Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Hrbitov_zviratek King S. horor 1994 250 654789
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
My_deti_ze_stanice_ZOO Felscherinov C.V. roman 2005 150 36987
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Ja_robot Asimov I. sci-fi 1993 250 123456
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Robinson_Crusoe Defoe D. dobrodruzne 1961 150 35489
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Saturnin Jirotka Z. humor 2008 99 369852
Romany jsou:
My_deti_ze_stanice_ZOO

Knihy s cenou mensi nez 300,- Kc jsou:
Hrbitov_zviratek
My_deti_ze_stanice_ZOO
Ja_robot
Robinson_Crusoe
Saturnin

Prijmeni vsech autoru jsou:
King
Felscherinov
Asimov
Defoe
Jirotka
________________________________________

Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Kmotr Puzo M. roman 1990 150 568953
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Temna_vez King S. sci-fi 2010 300 22556
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Pycha_a_predsudek Austen J. romany 2008 250 398456
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Lolita Nabokov V. roman 2007 300 25478
Zadej nazev knihy, prijmeni autora, jmeno autora, zanr knihy, rok vydani, cenu a id:
Zaklinac_1 Sapkowski A. sci-fi 1999 250 2458565
Romany jsou:
Kmotr
Lolita

Knihy s cenou mensi nez 300,- Kc jsou:
Kmotr
Pycha_a_predsudek
Zaklinac_1

Prijmeni vsech autoru jsou:
Puzo
King
Austen
Nabokov
Sapkowski
________________________________________
				</div>
			</div>

			<div class="row">
			<H5>12 - Struktury - pacienti</H5>
				Realizujte program, který z pěti daných pacientů vybere všechny pacienty s nemocí tbc a všechny pacienty s pojišťovnou 211. Nakonec vypíše všechna příjmení pacientů.
				<br /><br />
				Vstupem programu jsou příjmení, jméno, rodné číslo, nemoc a zdravotní pojišťovna pacienta. Jméno, příjmení a diagnóza jsou řetězce (max. 50 znaků), rodné číslo a zdravotní pojišťovna jsou celá čísla.
				<br /><br />
				Výstupem programu jsou následující informace v pořadí dle ukázek:
				<br /><br />
				- jméno a příjmení pacientů s tbc,<br />
				- jméno a příjmení pacientů s pojišťovnou 211,<br />
				- příjmení všech pacientů.<br />
				Dodržte přesně formát všech výpisů. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu (a za případným chybovým hlášením). Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupů/výstupů k testování Vašeho programu.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém).
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
________________________________________

Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Novak Jan 5750113312 tbc 211
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Malinkata Ursula 6251139924 tbc 111
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Holy Tomas 5404093897 tbc 222
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Maly Xavier 6901013225 tbc 205
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Schwarzova Ludmila 7756149945 tbc 213
Jmeno a prijmeni pacienta s tbc:
Jan Novak
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Jan Novak
Jmeno a prijmeni pacienta s tbc:
Ursula Malinkata
Jmeno a prijmeni pacienta s tbc:
Tomas Holy
Jmeno a prijmeni pacienta s tbc:
Xavier Maly
Jmeno a prijmeni pacienta s tbc:
Ludmila Schwarzova
Prijmeni vsech pacientu jsou:
Novak
Malinkata
Holy
Maly
Schwarzova
________________________________________

Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Novakova Jana  3756111318 nestovice 205
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Ondrak Matej 1025367810 nestovice 211
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Surny Jakub 3691215181 nestovice 213
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Hotovy Karel 2136457890 nestovice 212
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Samarova Ludmila 2013597825 nestovice 212
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Matej Ondrak
Prijmeni vsech pacientu jsou:
Novakova
Ondrak
Surny
Hotovy
Samarova
________________________________________

Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Hubova Jana 1026893247 cholera 211
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Kamarad Jan 9685236741 cholera 213
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Hodny Antonin 980356301 cholera 213
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Malirova Dorota 736587410 cholera 212
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Dobra Lenka 3678214510 cholera 205
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Jana Hubova
Prijmeni vsech pacientu jsou:
Hubova
Kamarad
Hodny
Malirova
Dobra
________________________________________

Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Hruba Kamila 3693693612 zloutenka 205
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Stoural Havel 2013846361 zloutenka 212
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Lukavska Sofie 2223336689 zloutenka 213
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Modra Libuse 4478963752 zloutenka 211
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Smutny Petr 8246930178 zloutenka 205
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Libuse Modra
Prijmeni vsech pacientu jsou:
Hruba
Stoural
Lukavska
Modra
Smutny
________________________________________

Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Hodlova Karla 2530194752 chripka 211
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Pokorny Daniel 1597534562 chripka 212
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Podla Katerina 3698521470 chripka 213
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Mala Simona 3416285790 chripka 205
Zadejte prijmeni, jmeno, rodne cislo, nemoc a zdravotni pojistovnu pacienta:
Sikovny Pavel 8974236852 chripka 211
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Karla Hodlova
Jmeno a prijmeni pacientu s pojistovnou 211 je:
Pavel Sikovny
Prijmeni vsech pacientu jsou:
Hodlova
Pokorny
Podla
Mala
Sikovny
________________________________________
				</div>
			</div>				
		</div>
	</div>
	<div class="section no-pad-bot" id="home_exercise">
		<div class="container">
			<br /><br />
			<H4> Domácí úlohy </H4>
				<ul id="dropdown3" class="dropdown-content light-blue lighten-1">
					<!--<li><a href="#home1" class="white-text">1<span class="new red badge"></a></li>-->
					<li><a href="#home1" class="white-text">1</a></li>
					<li><a href="#home2" class="white-text">2</a></li>
					<li><a href="#home3" class="white-text">3<span class="new red badge"></span></a></li>
				</ul>
				<a class="btn dropdown-button light-blue lighten-1 grey-text text-lighten-5" href="#!" data-activates="dropdown3">Úloha<i class="mdi-navigation-arrow-drop-down right"></i></a>
				<div class="row" id=home1>
					<br /><br />
					<H5> 1 - Obsah a obvod 2D obrazců  </H5>
					
					Realizujte program, který vypočítá podle volby obsah a obvod rovinného obrazce.
					<br /><br />
					Vstupem programu je znak 'a', 'b' nebo 'c', který označuje volbu uživatele - a - čtverec, b - obdélník, c - kruh, a hodnoty nutné pro výpočet obvodu a obsahu zvoleného obrazce.
					<br /><br />
					Výstupem programu jsou následující informace v pořadí dle ukázek:
					<br /><br />
					 - obsah obrazce a<br />
					 - obvod obrazce.<br />
					Program detekuje chybu, zobrazí chybové hlášení dle ukázky a ukončí se, pokud jsou na vstupu nečíselné hodnoty nebo pokud rozměry (stran, poloměr) nejsou kladná (desetinná) čísla. Chybové hlášení vypisujte na standardní výstup (nevypisujte jej na standardní chybový výstup).
					<br /><br />
					Dodržte přesně formát všech výpisu. Výpis Vašeho programu musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje výpis na přesnou shodu. Pokud se výpis Vašeho programu liší od referenčního výstupu, je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu a za případným chybovým hlášením. Využijte přiložený archiv s testovacími vstupy a výstupy a přečtěte si sekci FAQ, jak využít přesměrování vstupu/výstupu k testování Vašeho programu.
					<br /><br />
					
					<b>Ukázka práce programu:</b>
					<div style="white-space: pre">
Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
a
Zadejte stranu ctverce:
5.5
Obsah ctverce je: 30.2500
Obvod ctverce je: 22.0000

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
b
Zadejte strany obdelniku:
2 3
Obsah obdelniku je: 6.0000
Obvod obdelniku je: 10.0000

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
c
Zadejte polomer kruznice:
10
Obsah kruznice je: 314.1593
Obvod kruznice je: 62.8319

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
a
Zadejte stranu ctverce:
-8
Nespravny vstup.

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
a
Zadejte stranu ctverce:
hello
Nespravny vstup.

Zadejte rovinny obrazec, jehoz obsah a obvod chcete spocitat: a - ctverec, b - obdelnik, c - kruh
d
Nespravny vstup.
				</div>
			</div>
				<div class="row" id=home2>
				<br /><br />
				<H5> 2 - Součet binárních čísel   </H5>
				Sečtěte dvě čísla a vypište jejich součet, vše ve dvojkové soustavě.
				<br /><br />
				Vstupem programu jsou dvě čísla ve dvojkové soustavě, čísla jsou oddělena bílým znakem. Pokud vstup obsahuje jiné, než dvojkové hodnoty či oddělovače, jedná se o chybu. Čísla jsou zadaná obvyklým způsobem zleva doprava (nejvyšší řád je vlevo).
				<br /><br />
				Výstupem programu je dvojkové číslo, které je součtem vstupních hodnot. Ve výpisu potlačte nevýznamné nuly vlevo.
				<br /><br />
				Chybné vstupy program detekuje. reaguje na ně výpisem chybového hlášení a ukončením.
				<br /><br />
				
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
Zadejte dve binarni cisla:
10101 101001
Soucet: 111110

Zadejte dve binarni cisla:
1111100 100
Soucet: 10000000

Zadejte dve binarni cisla:
101 101
Soucet: 1010

Zadejte dve binarni cisla:
11111 111
Soucet: 100110

Zadejte dve binarni cisla:
001 000001
Soucet: 10

Zadejte dve binarni cisla:
10020 1001a0
Nespravny vstup.

Zadejte dve binarni cisla:
abraka1dabra0 1fuj0tajksl1
Nespravny vstup.
				</div>
			</div>
			<div class="row" id=home3>
				<br /><br />
				<H5> 3 - Velikonoce, velikonoce přicházejí, ...  </H5>
				Realizujte funkci easterReport, která pro zadané roky zjisti data Velikonoc. Funkce uloží výsledný den, měsíc (v textové podobě) a rok jako řádek tabulky do výstupního HTML souboru. Nepovinné: funkci a veškeré pomocné funkce zdrojového souboru okomentujte pomocí nástroje Doxygen.
				<br /><br />
				Úkolem je realizovat funkci s následujícím rozhraním:
				<br /><br />
				<b>int easterReport ( const char * years, const char * outFileName )</b>
				<br /><b>years</b><br />
				&emsp;je vstupní parametr vaší funkce. Jedná se o řetězec, který určuje roky, pro které má funkce vypočítat data Velikonoc. Roky mohou být zadané buď jednotlivě nebo jako intervaly. řetězec může obsahovat více požadovaných roků/intervalů, takové jsou oddělené čárkou. Bílé znaky mimo letopočty ignorujte. Každý rok musí být vetší než 1582 (přechod na Gregoriánský kalendář) a menší než 2200. Pokud je zadaný interval, pak počáteční rok intervalu musí být menší nebo roven koncovému roku intervalu. Pokud je zadaný nesprávný vstup, funkce nic nedělá a vrací odpovídající chybový kód (viz níže).
				<br /><br /><b>outFileName</b><br />
				&emsp;je řetězec se jménem výstupního souboru. Jméno souboru se může skládat z číslic, písmen, znaků tečka, dopředné a zpětné lomítko. Výstupní soubor navíc musí mít příponu ".html". Pokud jméno výstupního souboru nesouhlasí, funkce nic nedělá a vrací odpovídající chybový kód (viz níže). Pokud je jméno souboru správně, funkce do něj vygeneruje požadovaný HTML výstup. Pro každý z roků uvedených na vstupu funkce bude existovat jeden řádek tabulky v tomto vygenerovaném souboru.
				<br /><br /><b>návratová hodnota</b><br />
				&emsp;návratovou hodnotou funkce je rozlišení úspěchu/neúspěchu volání. Jsou definované následující chybové kódy:
				<br />
				&emsp;&emsp; - EASTER_OK pro úspěšné dokončení funkce.<br />
				&emsp;&emsp; - EASTER_INVALID_FILENAME bude vráceno, pokud jméno výstupního souboru neodpovídá kritériím výše.<br />
				&emsp;&emsp; - EASTER_INVALID_YEARS bude vráceno pokud vstupní řetězec nebyl správný (obsahoval špatně formátované roky/intervaly/nečíselné hodnoty ...).<br />
				&emsp;&emsp; - EASTER_IO_ERROR bude vráceno, pokud při zpracování výstupního souboru došlo k I/O chybě.
				<br />
				&emsp;Funkce nejprve kontroluje správnost jména výstupního souboru. Teprve pokud projde, kontroluje se řetězec s roky. Tedy pokud by funkce byla zavolaná s neplatným seznamem roků i s neplatným jménem souboru, vracela by hodnotu EASTER_INVALID_FILENAME.
				<br /><br />
				Algoritmus pro výpočet (Meeus/Jones/Butcher algorithm): Zadaný rok z Gregoriánského kalendáře označme Y. Pro výpočet data Velikonoc je použit následující algoritmus:<br />
				1. Y vydělíme 19 a získáme podíl (ten ignorujeme) a zbytek po dělení označíme A. To je pozice roku v 19-letém lunárním cyklu. (A+1 je tzv. Zlaté číslo)<br />
				2. Y vydělíme 100 a získáme podíl B a zbytek C<br />
				3. B vydělíme 4 a získáme podíl D a zbytek E<br />
				4. B + 8 vydělíme 25 a získáme podíl F<br />
				5. (B - F + 1) vydělíme 3 a získáme podíl G<br />
				6. 19A + B – D – G + 15 vydělíme 30 a získáme podíl (ignorujeme) a zbytek H<br />
				7. C vydělíme 4 a získáme podíl I a zbytek K<br />
				8. (32 + 2E + 2I - H - K) vydělíme 7 a získáme podíl (ignorujeme) a zbytek L<br />
				9. (A + 11H + 22L) vydělíme 451 a získáme podíl M<br />
				10. (H + L - 7M + 114) vydělíme 31 a získáme podíl N a zbytek P.<br />
				11. Velikonoční neděle je (P+1)-tý den a N-tý měsíc (N=3 pro březen a N=4 pro duben) v roce Y.
				<br /><br />
				pozn.: Je použito celočíselné dělení.
				<br /><br />
				Odevzdávejte zdrojový kód obsahující Vaší implementaci požadované funkce easterReport. V odevzdávaném zdrojovém souboru musí být implementace této funkce a implementace všech Vašich dalších pomocných funkcí. Vkládání hlavičkových souborů a funkci main ponechte v bloku podmíněného překladu, jak je ukázáno v následující ukázce. Doporučujeme zkopírovat si šablonu do Vašeho projektu a doplnit požadované implementace funkcí.

				<div style="white-space: pre">
#ifndef __PROGTEST__
#include &ltcstdio&gt
#include &ltcstdlib&gt
#include &ltcctype&gt
#include &ltcstring&gt
#include &ltiostream&gt
#include &ltiomanip&gt
#include &ltfstream&gt
#include &ltsstream&gt
#include &ltstring&gt
using namespace std;

#define EASTER_OK                0
#define EASTER_INVALID_FILENAME  1
#define EASTER_INVALID_YEARS     2
#define EASTER_IO_ERROR          3


#endif /* __PROGTEST__ */

int easterReport ( const char * years, const char * outFileName )
 {
   /* todo */
 }

#ifndef __PROGTEST__
int main ( int argc, char * argv[] )
 {
   /* tests */
   return 0;
 }
#endif /* __PROGTEST__ */
				</div>
				Dodržte přesně formát výstupního souboru i chybových hlášení Výstupní soubor musí přesně odpovídat ukázkám. Testování provádí stroj, který kontroluje obsah na přesnou shodu. Pokud se obsah Vašeho výstupního souboru liší od referenčního výstupu,je Vaše odpověď považovaná za nesprávnou. Záleží i na mezerách, i na odřádkování. Nezapomeňte na odřádkování za posledním řádkem výstupu. Využijte přiložený archiv obsahující požadovaný výstup pro ukázkové vstupy.
				<br /><br />
				Váš program bude spouštěn v omezeném testovacím prostředí. Je omezen dobou běhu (limit je vidět v logu referenčního řešení) a dále je omezena i velikost dostupné paměti (ale tato úloha by ani s jedním omezením neměla mít problém). Testovací prostředí dále zakazuje používat některé "nebezpečné funkce" -- funkce pro spouštění programu, pro práci se sítí, ... Pokud jsou tyto funkce použité, program se nespustí.
				<br /><br />
				<b>Ukázka práce programu:</b>
				<div style="white-space: pre">
easterReport ( "2012,2013,2015-2020", "out0.html" ); /* return: 0 */
/* file out0.html:
------8&lt-----
&lt!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"&gt
&lthtml&gt
&lthead&gt
&ltmeta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt
&lttitle&gtC++&lt/title&gt
&lt/head&gt
&ltbody&gt
&lttable width="300"&gt
&lttr&gt&ltth width="99"&gtden&lt/th&gt&ltth width="99"&gtmesic&lt/th&gt&ltth width="99"&gtrok&lt/th&gt&lt/tr&gt
&lttr&gt&lttd&gt8&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2012&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt31&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2013&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt5&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2015&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt27&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2016&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt16&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2017&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt1&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2018&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt21&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2019&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt12&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2020&lt/td&gt&lt/tr&gt
&lt/table&gt
&lt/body&gt
&lt/html&gt
------8&lt-----
*/

easterReport ( "2000 - 2014", "out1.html" ); /* return: 0 */
/* file out1.html:
------8&lt-----
&lt!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"&gt
&lthtml&gt
&lthead&gt
&ltmeta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt
&lttitle&gtC++&lt/title&gt
&lt/head&gt
&ltbody&gt
&lttable width="300"&gt
&lttr&gt&ltth width="99"&gtden&lt/th&gt&ltth width="99"&gtmesic&lt/th&gt&ltth width="99"&gtrok&lt/th&gt&lt/tr&gt
&lttr&gt&lttd&gt23&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2000&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt15&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2001&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt31&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2002&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt20&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2003&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt11&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2004&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt27&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2005&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt16&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2006&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt8&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2007&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt23&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2008&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt12&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2009&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt4&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2010&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt24&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2011&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt8&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2012&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt31&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2013&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt20&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2014&lt/td&gt&lt/tr&gt
&lt/table&gt
&lt/body&gt
&lt/html&gt
------8&lt-----
*/

easterReport ( "2001 , 2002  ,  2003 ,2005,2006", "out2.html" ); /* return: 0 */
/* file out2.html:
------8&lt-----
&lt!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"&gt
&lthtml&gt
&lthead&gt
&ltmeta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt
&lttitle&gtC++&lt/title&gt
&lt/head&gt
&ltbody&gt
&lttable width="300"&gt
&lttr&gt&ltth width="99"&gtden&lt/th&gt&ltth width="99"&gtmesic&lt/th&gt&ltth width="99"&gtrok&lt/th&gt&lt/tr&gt
&lttr&gt&lttd&gt15&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2001&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt31&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2002&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt20&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2003&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt27&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2005&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt16&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2006&lt/td&gt&lt/tr&gt
&lt/table&gt
&lt/body&gt
&lt/html&gt
------8&lt-----
*/

easterReport ( "2000,2011,2010-2020", "out3.html" ); /* return: 0 */
/* file out3.html:
------8&lt-----
&lt!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"&gt
&lthtml&gt
&lthead&gt
&ltmeta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt
&lttitle&gtC++&lt/title&gt
&lt/head&gt
&ltbody&gt
&lttable width="300"&gt
&lttr&gt&ltth width="99"&gtden&lt/th&gt&ltth width="99"&gtmesic&lt/th&gt&ltth width="99"&gtrok&lt/th&gt&lt/tr&gt
&lttr&gt&lttd&gt23&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2000&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt24&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2011&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt4&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2010&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt24&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2011&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt8&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2012&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt31&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2013&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt20&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2014&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt5&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2015&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt27&lt/td&gt&lttd&gtbrezen&lt/td&gt&lttd&gt2016&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt16&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2017&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt1&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2018&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt21&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2019&lt/td&gt&lt/tr&gt
&lttr&gt&lttd&gt12&lt/td&gt&lttd&gtduben&lt/td&gt&lttd&gt2020&lt/td&gt&lt/tr&gt
&lt/table&gt
&lt/body&gt
&lt/html&gt
------8<-----
*/

easterReport ( "2000-3000", "out4.html" ); /* return: 2 */
				</div>
			</div>
		</div>
	</div>
  
	<div class="section no-pad-bot" id="functions">
		<div class="container">
			<br /><br />
			<H4> Užitečné funkce </H4>
				<div class="row">
					<table class="responsive-table">
						<thead>
						  <tr>
							  <th>funkce</th>
							  <th>popis</th>
						  </tr>
						</thead>

						<tbody>
						  <tr>
							<td>getline(cin, name)</td>
							<td>načte celý řádek (stirng s; cin >> s; // načte text pouze do první mezery)</td>
						  </tr>
						  <tr>
							<td>int i = 123; int *ptr_i; ptr_i = &i; *ptr_i = 456; cout << i; //456 </td>
							<td>práce s ukazateli</td>
						  </tr>
						  <tr>
							<td>setw(10) z knihovny <iomanip></td>
							<td>zarovná číslo na 10 pozic; cout << setw(10) << 77 << endl; //"8mezer"77</td>
						  </tr>
						  <tr>
							<td>cin.fail()</td>
							<td>true, pokud selže cin. Např. int i;cin >> i; // => 'cin.fail()' bude true, pokud na vstupu bude nečíselná hodnota</td>
						  </tr>
						  <tr>
							<td>cin.peek()</td>
							<td>vrátí znak ze vztupu, aniž by jej zpracoval "jen se na něj podívá"</td>
						  </tr>
						  <tr>
							<td>cin.clear()</td>
							<td>odstrani chybu - musíme se s ni sami vyporadat</td>
						  </tr>
						  <tr>
							<td>cin.ignore(256, '\n')</td>
							<td>ignorují se všechny znaky (až 256) až dokud nedojde k odřádkování</td>
						  </tr>
						  <tr>
							<td><a href = "http://www.cplusplus.com/reference/cmath/round/">round(double x)</a> z knihovny &ltmath.h&gt</td>
							<td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
						  </tr>
						  <tr>
							<td>float roundf(float x)</a> z knihovny &ltmath.h&gt</td>
							<td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
						  </tr>
						  <tr>
						  <tr>
							<td>long double roundl(long double x)</a> z knihovny &ltmath.h&gt</td>
							<td>round(5.5) dá hodnotu 6, round(-5.5) dá hodnotu -6</td>
						  </tr>
						  <tr>
							<td>floor() z knihovny &ltmath.h&gt</td>
							<td>floor(3.8) dá hodnotu 3, floor(-3.8) dá hodnotu -4</td>
						  </tr>
						  <tr>
							<td>ceil() z knihovny &ltmath.h&gt</td>
							<td>ceil(3.2) dá hodnotu 4, ceil(-3.2) dá hodnotu -3</td>
						  </tr>
						  <tr>
							<td>trunc() z knihovny &ltmath.h&gt</td>
							<td>trunc(3.2) dá hodnotu 3, ceil(-3.2) dá hodnotu -3</td>
						  </tr>
						  <tr>
							<td><i>myString</i>.find('c')</td>
							<td>Vrací pozici prvního nalezeného znaku 'c' ve stringu <i>myString</i> nebo '-1' (pokud nenalezen)</td>
						  </tr>
						  <tr>
							<td>slovo1.compare(slovo2)</td>
							<td>Porovná 2 slova a vrací 0 pokud se neliší, zápornou hodnotu pokud první odlišný znak ve "slovo1" < znak na téže pozici ve "slovo2", nebo pokud se znaky neliší, ale "slovo1" je kratší. Kladnou hodnotu funkce vrací ve zbývajících případech</td>
						  </tr>
						  <tr>
							<td>int *fieldA = new int[pocetA];</td>
							<td>Vytvoří ukazatel na pole integerů o velikosti "pocetA" prvků</td>
						  </tr>
						  <tr>
							<td>vector &ltdouble&gt myVector</td>
							<td>Vektor prvků typu double</td>
						  </tr>
						  <tr>
							<td>myVector.push_back(item)</td>
							<td>Přidá nový element na konec vektoru</td>
						  </tr>
						  <tr>
							<td>ifstream infile;<br/> infile.open(srcFileName);<br/> infile >> i;<br/> infile.close()</td>
							<td>otevře soubor, uloží int do proměnné i, zavře soubor; #include &ltfstream&gt</td>
						  </tr>
						  <tr>
							<td>ofstream outfile;<br/> outfile.open(dstFileName);<br/> dstfile << i;<br/> outfile.close()</td>
							<td>otevře soubor, uloží do něj proměnnou i, zavře soubor; #include &ltfstream&gt</td>
						  </tr>
						  <tr>
							<td>while(!file.eof())</td>
							<td>Dokud není konec souboru, tak...</td>
						  </tr>
						</tbody>
					</table>
				</div>
		</div>
	</div>

  <footer class="page-footer orange">
    <div class="container">

		<div class="row">
	  
		</div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Ing. Jiří Znoj - jiri.znoj {zavinac} vsb.cz .......... ( web page use <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a> )
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-87291152-1', 'auto');
	ga('send', 'pageview');
  </script>
  </body>
</html>
