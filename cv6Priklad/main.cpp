#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace std;

int main() {
 string a = "Hobit, aneb cesta tam";
 string b("a zase zpatky.");

 string nazev = a + " " + b;

 string zahlavi(nazev.size(), '*');
 string zapati = zahlavi;
 replace(zapati.begin(), zapati.end(), '*', '+');

 string pozpatku(nazev);
 reverse(pozpatku.begin(), pozpatku.end());

 string prvni_slovo(a.begin(), find(a.begin(), a.end(), ' '));

 int pozice = find(a.begin(), a.end(), ',') - a.begin();
 string prvni_slovo_OK(a.substr(0, pozice));

 cout << zahlavi << endl << nazev << endl
      << pozpatku << endl << prvni_slovo
      << '\t' << prvni_slovo_OK
      << endl << zapati << endl;
 return 0;
} // int main()
