#ifndef __PROGTEST__
#include <iostream>
#include <fstream>
using namespace std;
#endif
int evenOdd ( const char * srcFileName, const char * dstFileName )
 {
    ifstream in (srcFileName, ifstream::in);
    ofstream out (dstFileName, ofstream::out);
    int number;
    in >> number;
    while(!in.eof()){
        if(number % 2 == 0)
            out << number << "\n";
        in >> number;
        if(in.fail() && !in.eof()){
            return 0;
        }
    }
    in.close();
    ifstream in2 (srcFileName, ifstream::in);
    in2 >> number;
    while(!in2.eof()){
        if(number % 2 != 0)
            out << number << "\n";
        in2 >> number;
        if(in.fail() && !in.eof()){
            return 0;
        }
    }
    /*char c = ifs.get();
    while (ifs.good()) {
        cout << c;
        c = ifs.get();
    }*/
    in2.close();
    out.close();
    return 1;
 }
#ifndef __PROGTEST__
int main ()
{
    return evenOdd( "text.txt", "output.txt");
}
#endif