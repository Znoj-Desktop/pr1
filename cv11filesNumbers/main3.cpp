#ifndef __PROGTEST__
#include <iostream>
#include <cstdlib>
#endif /* __PROGTEST__ */

/* Vase pomocne funkce (jsou-li potreba) */
#include <fstream>
#include <string>
#include <cctype>

using namespace std;

int evenOdd ( const char * srcFileName, const char * dstFileName )
 {
    ifstream infile;
    ofstream outfile;
    infile.open(srcFileName);
    outfile.open(dstFileName);
    if(infile == NULL || outfile == NULL){
        //cout << "Chyba" << endl;
        return 0;
    }

    int i = 0;
    int *oddNumbers = new int[10240];
    int counterOdd = 0;
    int *evenNumbers = new int[10240];
    int counterEven = 0;

    while(infile.peek() != EOF){
        infile >> i;
        if(infile.fail() && !in.eof()){
            return 0;
        }
        if(i%2 == 0){
            evenNumbers[counterEven++] = i;
        }
        else{
            oddNumbers[counterOdd++] = i;
        }
    }
    for(int i = 0; i < counterEven; i++){
        outfile << evenNumbers[i] << endl;
    }

    for(int i = 0; i < counterOdd; i++){
        outfile << oddNumbers[i] << endl;
    }
    delete oddNumbers;
    delete evenNumbers;
    infile.close();
    outfile.close();
    return 1;
 }

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
 {
   /* Vase testy */
    evenOdd("input", "output");
    return 0;
 }
#endif /* __PROGTEST__ */
