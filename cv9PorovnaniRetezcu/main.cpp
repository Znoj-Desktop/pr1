#include <iostream>

using namespace std;

string tolower(string in){
    for(unsigned int i = 0; i < in.length(); i++){
        if(in[i]<='Z' && in[i]>='A'){
            in[i] -= ('Z'-'z');
        }
    }
    return in;
}

int main()
{
	//do{
    string slovo1 = "";
    string slovo2 = "";
    string slovo3 = "";
    cout << "Zadejte 3 slova:" <<endl;

    cin >> slovo1 >> slovo2 >> slovo3;
    if(cin.fail() || slovo3.length() == 0){
        cout << "Nespravny vstup." << endl;
        //cin.clear(); //odstrani chybu a budeme se s ni snazit vyporadat
        //cin.ignore(256, '\n'); //ignorujeme vsechny znaky (az 256), nebot jsou chybne, az dokud nedojde k odradkovani
        //continue;
        return 0;
    }
    slovo1 = tolower(slovo1);
    slovo2 = tolower(slovo2);
    slovo3 = tolower(slovo3);

    cout << "Slovo1 a slovo2";
    if(slovo1.compare(slovo2) == 0){
        cout << " jsou ";
    }
    else{
        cout << " nejsou ";
    }
    cout << "stejna." << endl;

    cout << "Slovo1 a slovo3";
    if(slovo1.compare(slovo3) == 0){
        cout << " jsou ";
    }
    else{
        cout << " nejsou ";
    }
    cout << "stejna." << endl;

    cout << "Slovo2 a slovo3";
    if(slovo2.compare(slovo3) == 0){
        cout << " jsou ";
    }
    else{
        cout << " nejsou ";
    }
    cout << "stejna." << endl;



    cout << "Pocet znaku ve slovo1 je: " << slovo1.length() << endl;
    cout << "Pocet znaku ve slovo2 je: " << slovo2.length() << endl;
    cout << "Pocet znaku ve slovo3 je: " << slovo3.length() << endl;

	//}while(cin.peek() != -1);
    return 0;
}
