#include <iostream>

using namespace std;

int main()
{
    double i;

    cout << "Zadejte cislo:" << endl;
    cin >> i;
    if(cin.fail() || (cin.peek() != 10)){
        cout << "Chybne zadani." <<endl;;
        return 0;
    }
    cout << i << " : Spravne zadane cislo." << endl;
    return 0;
}
