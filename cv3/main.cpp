#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    cout << "Zadejte souradnice bodu A a B:" << endl;
    double Ax, Ay, Bx, By;
    cin >> Ax;
    cin >> Ay;
    cin >> Bx;
    cin >> By;

    cout << "X-ova souradnice stredu S je: " << (Ax + Bx) / 2 << endl;
    cout << "Y-ova souradnice stredu S je: " << (Ay + By) / 2 << endl;
    double Sv = sqrt((Bx - Ax) * (Bx - Ax) + (By - Ay) * (By - Ay));
    cout << "Smerovy vektor u ma delku: " << Sv << endl;
    cout << "Polomer kruznice r ma hodnotu: " << Sv/2 << endl;
    return 0;
}
